<%@page import="java.util.Enumeration"%>
<%@page import="base.entity.User"%>
<%@page import="base.entity.Group"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<form action="HServlet">
		<h3>Users</h3>
		<table border="3">
			<tr>
				<td>Id</td>
				<td>Name</td>
				<td>GroupId</td>
			</tr>
			<%
			Enumeration<String> users = session.getAttributeNames();
			while(users.hasMoreElements()){ 
				String key = (String) users.nextElement();
				if(!key.endsWith(")")){
					User user = (User) session.getAttribute(key);%>
			<tr>
				<td><%= user.getId() %></td>
				<td><%= user.getName() %></td>
				<td><%= user.getGroupId() %></td>
			</tr>
			<%	}
			}%>
		</table>
		<h3>Groups</h3>
		<table border="3">
			<tr>
				<td>Id</td>
				<td>Name</td>
			</tr>
			<%
			Enumeration<String> groups = session.getAttributeNames();
			while(groups.hasMoreElements()){ 
				String key = (String) groups.nextElement();
				if(key.endsWith(")")){
					Group group = (Group) session.getAttribute(key);%>
			<tr>
				<td><%= group.getId() %></td>
				<td><%= group.getName() %></td>
			</tr>
				<%}
			}%>
		</table>
		<hr></hr>
		<p><a href="index.jsp">Home</a></p>
	</form>
</body>
</html>