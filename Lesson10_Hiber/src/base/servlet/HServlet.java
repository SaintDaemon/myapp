package base.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.hibernate.StaleStateException;

import base.entity.Group;
import base.entity.User;
import base.manager.DBManager;

/**
 * Servlet implementation class HServlet
 */
public class HServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public HServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		List<User> listU = DBManager.getInstance().getUsersList();
		for(User u: listU){
			session.setAttribute(String.valueOf(u.getId()), u);
		}
		List<Group> listG = DBManager.getInstance().getGroupsList();
		for(Group g: listG){
			session.setAttribute(String.valueOf(g.getId()) + ")", g);
		}
		
		//add user
		String idGroup = request.getParameter("idGroup");
		String nameU = request.getParameter("nameU");
		
		if(nameU != null && idGroup != null){
			if(!"".equals(nameU) && !"".equals(idGroup)){
				Integer idG = null;
				try{
					idG = Integer.parseInt(idGroup);			
				} catch (NumberFormatException e){
					System.out.println(e);
				}
				if(idG != null){
					User user = new User();
					user.setName(nameU);
					user.setGroupId(idG);
					DBManager.getInstance().addUser(user);
					session.setAttribute(String.valueOf(user.getId()), user);
				}
			}
		}
		
		//add group
		String nameG = request.getParameter("nameG");
		
		if(!"".equals(nameG) && nameG != null){
			Group group = new Group();
			group.setName(nameG);
			DBManager.getInstance().addGroup(group);
			session.setAttribute(String.valueOf(group.getId()) + ")", group);
		}
		
		//delete user
		String idU = request.getParameter("IdU");
		
		if(idU != null && !"".equals(idU)){
			Integer idUs = null;
			try{
				idUs = Integer.parseInt(idU);			
			} catch (NumberFormatException e){
				System.out.println(e);
			}
			if(idUs != null){
				User user = new User();
				user.setId(idUs);
				try{
					DBManager.getInstance().removeUser(user);
					session.removeAttribute(String.valueOf(user.getId()));
				} catch(StaleStateException e){
					System.out.println(e);
				}
			}
		}
		
		//delete group
		String idG = request.getParameter("IdG");
		
		if(idG != null && !"".equals(idG)){
			Integer idGr = null;
			try{
				idGr = Integer.parseInt(idG);			
			} catch (NumberFormatException e){
				System.out.println(e);
			}
			if(idGr != null){
				Group group = new Group();
				group.setId(idGr);
				try{
					DBManager.getInstance().removeGroup(group);
					session.removeAttribute(String.valueOf(group.getId()) + ")");
				} catch(StaleStateException e){
					System.out.println(e);
				} 
			}
		}
		
		//update user
		String idUs = request.getParameter("IdUser");
		String nameUs = request.getParameter("NameU");
		String idGr = request.getParameter("idGr");
		
		if(idUs != null && !"".equals(idUs)){
			Integer idUse = null;
			try{
				idUse = Integer.parseInt(idUs);
			} catch (NumberFormatException e){
				System.out.println(e);
			} 
			if(idUse != null){
				User user = new User();
				user.setId(idUse);
				if(!"".equals(idGr)){
					Integer idGro = null;
					try{
						idGro = Integer.parseInt(idGr);
						user.setGroupId(idGro);
					} catch (NumberFormatException e){
						System.out.println(e);
					}
				}
				if(!"".equals(nameUs)){
					user.setName(nameUs);
				}
				try{
					DBManager.getInstance().updateUser(user);
					session.setAttribute(String.valueOf(user.getId()), user);
				} catch (StaleStateException e){
					System.out.println(e);
				}
			}
		}
		
		//update group
		String nameGr = request.getParameter("NameGro");
		String idGro = request.getParameter("IdGro");
		
		if(idGro != null && !"".equals(idGro)){
			Integer idGrou = null;
			try{
				idGrou = Integer.parseInt(idGro);
			} catch (NumberFormatException e){
				System.out.println(e);
			} 
			if(idGrou != null){
				Group group = new Group();
				group.setId(idGrou);
				if(nameGr != null && !"".equals(nameGr)){
					group.setName(nameGr);
				}
				try{
					DBManager.getInstance().updateGroup(group);	
					session.setAttribute(String.valueOf(group.getId()) + ")", group);
				} catch (StaleStateException e){
					System.out.println(e);
				}
			}
		}
		request.getRequestDispatcher("index.jsp").forward(request, response);
	}
}
