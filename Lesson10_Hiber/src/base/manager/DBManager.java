package base.manager;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;

import base.HibernateUtil;
import base.entity.Group;
import base.entity.User;

public class DBManager {

	private static DBManager instance;

	private DBManager() {}

	public static DBManager getInstance() {
		if (instance == null)
			instance = new DBManager();
		return instance;
	}
	
	public void addUser(User user) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		session.save(user);
		session.getTransaction().commit();
		session.close();
	}

	public void addGroup(Group group) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		session.save(group);
		session.getTransaction().commit();
		session.close();
	}

	public void removeUser(User user) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		session.delete(user);
		session.getTransaction().commit();
		session.close();
	}

	public void removeGroup(Group group) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		session.delete(group);
		session.getTransaction().commit();
		session.close();
	}
	
	public void updateUser(User user) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		session.update(user);
		session.getTransaction().commit();
		session.close();
	}
	
	public void updateGroup(Group group) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		session.update(group);
		session.getTransaction().commit();
		session.close();
	}
	
	public List<User> getUsersList(){
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		Criteria crit = session.createCriteria(User.class);
		crit.setMaxResults(50);
		List<User> users = crit.list();
		session.getTransaction().commit();
		session.close();
		return users; 
	}
	
	public List<Group> getGroupsList(){
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		Criteria crit = session.createCriteria(Group.class);
		crit.setMaxResults(50);
		List<Group> groups = crit.list();
		session.getTransaction().commit();
		session.close();
		return groups;
	}
}