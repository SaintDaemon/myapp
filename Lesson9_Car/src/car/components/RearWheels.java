package car.components;

import org.springframework.stereotype.Component;

import car.interfaces.Retarding;
import car.interfaces.StatusAware;

@Component
public class RearWheels implements Retarding, StatusAware {
	
	final private int brakingForce = 10;
	
	@Override
	public int brakingForce() {
		return -brakingForce;
	}

	@Override
	public void showStatus() {
		System.out.println("ForwardWheels brakingForce : " + brakingForce);	
	}
}
