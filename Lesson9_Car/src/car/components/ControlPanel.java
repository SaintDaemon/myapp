package car.components;

import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ControlPanel {
	
	private final int maxSpeed = 200;
	private double speed; 
	private double distanceTraveled; 
	
	private double lossOfFuel = 0.1;
	private int speedChange = 0;
	int time = 1;
	private ArrayList<Double> speedList;
	
	private double oldSpeed; 
	private double oldDistanceTraveled; 
	private double oldGas; 
	private boolean oldEngine = false; 
	private boolean oldHornStatus = false; 
	private boolean oldAccelerator = false;
	private int oldAngleSteeringWheel;
	private int oldAngleForwardWheels;
	private boolean oldHandBrake = false;
	private boolean oldBrakePedal = false;
	
	@Autowired private Engine engine;
	@Autowired private GasTank gasTank;
	@Autowired private Accelerator accelerator;
	@Autowired private Horn horn;
	@Autowired private BrakePedal brakePedal;
	@Autowired private ForwardWheels forwardWheels;
	@Autowired private HandBrake handBrake;
	@Autowired private RearWheels rearWheels;
	@Autowired private SteeringWheel steeringWheel;
	
	@PostConstruct
	public void init(){
		System.out.println("----------------------");
		engine.showStatus();
		gasTank.showStatus();
		accelerator.showStatus();
		brakePedal.showStatus();
		handBrake.showStatus();
		horn.showStatus();
		steeringWheel.showStatus();
		forwardWheels.showStatus();
		rearWheels.showStatus();
		System.out.println("----------------------");
	}
	
    public void showAverageSpeed() {
		double sum = 0;
        for(Double l : speedList){
        	sum = sum + l;
        }
        System.out.println("The average speed is " + (sum/time) + " km/h");
    }
	
	public void stopTheCar(){
		engine.setStatus(false);
		showAverageSpeed();
	}
	
	public void startTheCar(){
		if(gasTank.getCurrentVolume() != 0){
			engine.setStatus(true);
		}
		drive();
	}
	
	public void lossOfFuel(){
		if(gasTank.getCurrentVolume() > 0){
			gasTank.setCurrentVolume(gasTank.getCurrentVolume() - lossOfFuel);
		} else {
			stopTheCar();
		}
	}
	
	public void fueling(double num){
		if(num > 0 ){
			gasTank.setCurrentVolume(gasTank.getCurrentVolume() + num);
		}
	}
	
	public void signal(){
		horn.setStatus(true);
		Thread t2 = new Thread() {
			public void run() {
				try {
					sleep(2000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				horn.setStatus(false);
			}
		};
		t2.start();
	}
	
	public void speedUp(final int toSpeed){
		accelerator.setStatus(true);
		lossOfFuel = 0.2;
		speedChange = 10;
		Thread t2 = new Thread() {
			public void run() {
				while((speed < toSpeed) && (speed < maxSpeed)){
					try {
						sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				accelerator.setStatus(false);
			}
		};
		t2.start();
	}
	
	public void speedDown_HandBreak(final int toSpeed) throws InterruptedException{
		speedChange = rearWheels.brakingForce();
		handBrake.setStatus(true);
		Thread t2 = new Thread() {
			public void run() {
				while(speed > toSpeed){
					try {
						sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				speedChange = 0;
				handBrake.setStatus(false);
			}
		};
		t2.start();
	}
	
	public void speedDown_BrakePedal(final int toSpeed) throws InterruptedException{
		speedChange = rearWheels.brakingForce() + forwardWheels.brakingForce();
		brakePedal.setStatus(true);
		Thread t2 = new Thread() {
			public void run() {
				while(speed > toSpeed){
					try {
						sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				speedChange = 0;
				brakePedal.setStatus(false);
			}
		};
		t2.start();
	}
	
	public void turning(int angle){
		speedChange = -5;
		steeringWheel.setAngle(angle);
		forwardWheels.turn(angle/2);
		Thread t2 = new Thread() {
			public void run() {
				while((speed > 0) && forwardWheels.getAngle() != 0){
					try {
						sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				speedChange = 0;
			}
		};
		t2.start();
	}
	
	public void drive(){
		speedList = new ArrayList<>();
		Thread t2 = new Thread() {
			public void run() {
				while((gasTank.getCurrentVolume() > 0) && (engine.getStatus() != false)){
					try {
						sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					lossOfFuel();
					speed = speed + speedChange;
					speedList.add(speed);
					if(speed > 0){
						distanceTraveled = distanceTraveled + speed/3600;
						lossOfFuel = 0.2;
					}
					
					System.out.println("[" + time + "]--------------------");
					time++;
					if(speed != oldSpeed){
						System.out.println("speed = " + speed + (" km/h"));
						oldSpeed = speed;
					}
					if(distanceTraveled != oldDistanceTraveled){
						System.out.println("distanse = " + (distanceTraveled * 1000) + " meters");
						oldDistanceTraveled = distanceTraveled;
					}
					if(gasTank.getCurrentVolume() != oldGas){
						System.out.println("gas = " + gasTank.getCurrentVolume() + " liters");
						oldGas = gasTank.getCurrentVolume();
					}
					if(engine.getStatus() != oldEngine){
						engine.showStatus();
						oldEngine = engine.getStatus();
					}
					if(horn.getStatus() != oldHornStatus){
						horn.showStatus();
						oldHornStatus = horn.getStatus();
					}
					if(accelerator.getStatus() != oldAccelerator){
						accelerator.showStatus();
						oldAccelerator = accelerator.getStatus();
					}
					if(steeringWheel.getAngle() != oldAngleSteeringWheel){
						steeringWheel.showStatus();
						oldAngleSteeringWheel = steeringWheel.getAngle();
					}
					if(forwardWheels.getAngle() != oldAngleForwardWheels){
						System.out.println("ForwardWheels angle: " + forwardWheels.getAngle());
						oldAngleForwardWheels = forwardWheels.getAngle();
					}
					if(handBrake.getStatus() != oldHandBrake){
						handBrake.showStatus();
						oldHandBrake = handBrake.getStatus();
					}
					if(brakePedal.getStatus() != oldBrakePedal){
						brakePedal.showStatus();
						oldBrakePedal = brakePedal.getStatus();
					}
					System.out.println("--------------------");
				}
			}
		};
		t2.start();
	}
	
}