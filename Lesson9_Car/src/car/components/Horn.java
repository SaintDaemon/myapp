package car.components;

import org.springframework.stereotype.Component;

import car.interfaces.StatusAware;

@Component
public class Horn implements StatusAware {
	
	boolean status = false;
	
	public boolean getStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	@Override
	public void showStatus() {
		System.out.println("Horn status : " + status);
	}
}
