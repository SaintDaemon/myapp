package car.components;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import car.interfaces.StatusAware;

@Component
public class Accelerator implements StatusAware {
	
	private boolean status = false;
	
	public boolean getStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}
	
	@Override
	public void showStatus() {
		System.out.println("Accelerator status : " + status);
	}
}
