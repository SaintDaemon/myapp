package car.components;

import org.springframework.stereotype.Component;

import car.interfaces.StatusAware;

@Component
public class SteeringWheel implements StatusAware {
	
	private int maxAngleR = 180;
	private int maxAngleL = -180;
	private int angle;
	
	public int getAngle() {
		return angle;
	}

	public void setAngle(int an) {
		if((an > maxAngleR) || (an < maxAngleL)){
			angle = (an < maxAngleL) ? maxAngleL : maxAngleR;
		} else {
			angle = an;
		}
	}

	@Override
	public void showStatus() {
		System.out.println("SteeringWheel angle : " + angle);
	}
	
}
