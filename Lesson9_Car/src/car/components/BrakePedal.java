package car.components;

import org.springframework.stereotype.Component;

import car.interfaces.Retarding;
import car.interfaces.StatusAware;

@Component
public class BrakePedal implements StatusAware {
	
	private boolean status = false;
	
	public boolean getStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	@Override
	public void showStatus() {
		System.out.println("BrakePedal status : " + status);
	}
}
