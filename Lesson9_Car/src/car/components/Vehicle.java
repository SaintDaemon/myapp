package car.components;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import car.interfaces.StatusAware;

@Component
public class Vehicle implements StatusAware{
	
	@Autowired 
	private ControlPanel control;
	
	public ControlPanel getControlPanel() {
		return control;
	}

	@Override
	public void showStatus() {
		control.init();
	}
	
}
