package car.components;

import org.springframework.stereotype.Component;

import car.interfaces.Retarding;
import car.interfaces.StatusAware;
import car.interfaces.Steered;

@Component
public class ForwardWheels implements Retarding, Steered, StatusAware {
	
	final private int brakingForce = 10;
	private int maxAngleR = 90;
	private int maxAngleL = -90;
	private int angle;
	
	public int getAngle() {
		return angle;
	}

	@Override
	public void turn(int angle) {
		if((angle < maxAngleL) || (angle > maxAngleR)){
			this.angle = (angle < maxAngleL) ? maxAngleL : maxAngleR;
		} else {
			this.angle = angle;
		}
	}

	@Override
	public int brakingForce() {
		return -brakingForce;
	}

	@Override
	public void showStatus() {
		System.out.println("ForwardWheels angle: " + angle + "; brakingForce :" + brakingForce);		
	}
	
}
