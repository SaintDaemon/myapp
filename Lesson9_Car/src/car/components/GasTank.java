package car.components;

import org.springframework.stereotype.Component;

import car.interfaces.StatusAware;

@Component
public class GasTank implements StatusAware {
	
	private final double maxVolume = 50;
	private double currentVolume = 0;
	
	public double getCurrentVolume() {
		return currentVolume;
	}

	public void setCurrentVolume(double d) {
		currentVolume = d;
		if(currentVolume > maxVolume){
			currentVolume = maxVolume;
		}
	}

	@Override
	public void showStatus() {
		System.out.println("GasTank currentVolume: " + currentVolume + "/40");
	}

}
