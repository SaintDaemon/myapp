package car;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import car.components.ControlPanel;
import car.components.Engine;
import car.components.GasTank;
import car.components.Vehicle;

public class Test {
	
	public static void main(String[] args) throws InterruptedException {
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		Vehicle vehicle = context.getBean(Vehicle.class);
		
		ControlPanel cp = vehicle.getControlPanel();
		
		//...One minute from the life of the car)...
		cp.fueling(15);
		cp.startTheCar();
		Thread.sleep(2000);
		cp.signal();
		Thread.sleep(4000);
		cp.speedUp(70);
		Thread.sleep(10000);
		cp.speedDown_HandBreak(30);
		Thread.sleep(7000);
		cp.speedUp(100);
		Thread.sleep(10000);
		cp.speedDown_BrakePedal(40);
		Thread.sleep(7000);
		cp.speedUp(80);
		Thread.sleep(7000);
		cp.turning(30);
		Thread.sleep(9000);
		cp.turning(0);
		Thread.sleep(4000);
		cp.stopTheCar();
	}
}
