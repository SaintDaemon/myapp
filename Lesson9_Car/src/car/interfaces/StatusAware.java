package car.interfaces;

public interface StatusAware {
	
	public void showStatus();
}