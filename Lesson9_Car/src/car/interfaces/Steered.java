package car.interfaces;

public interface Steered {

	public void turn(int angle);
}