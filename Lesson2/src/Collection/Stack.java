package Collection;

import java.util.*;


class Stack implements Queue {
	private Object stack[] = new Object[1000];
	private int num = 0;
	
	int getNum(){
		return num;
	}
	
	@Override
	public boolean addAll(Collection c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void clear() {
		for(Object s: stack){
			s = null;
		}
		num = 0;
	}

	@Override
	public boolean contains(Object o) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean containsAll(Collection c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Iterator iterator() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean remove(Object o) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean removeAll(Collection c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean retainAll(Collection c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Object[] toArray() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object[] toArray(Object[] a) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean add(Object e) {
		stack[num] = e;
		num++;
		return true;
	}

	@Override
	public Object element() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean offer(Object e) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Object peek() {
		if(num == 0){
			return null;
		}else{
			return stack[num-1];
		}
	}

	@Override
	public Object poll() {
		if(num == 0){
			return null;
		}else{
			Object interim = stack[num-1];
			stack[num] = null;
			num--;
			return interim;
		}
	}

	@Override
	public Object remove() {
		// TODO Auto-generated method stub
		return null;
	}	
	
	public static void main(String args[]){
		Stack stack = new Stack();
		String s = "1";
		stack.add(s);
		
		String s2 = "2";
		stack.add(s2);
		
		String s3 = "3";
		stack.add(s3);
		
		String s4 = "4";
		stack.add(s4);
		
		System.out.println("num = " + stack.getNum());
		System.out.println("peec: stack[" + stack.getNum() + "] = " + stack.peek());
		System.out.println("num = " + stack.getNum());
		System.out.println("poll: stack[" + stack.getNum() + "] = " + stack.poll());
		System.out.println("num = " + stack.getNum());
		
		stack.clear();
		System.out.println(stack.peek());
		System.out.println(stack.poll());
	}	
}
