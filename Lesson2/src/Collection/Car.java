package Collection;

import java.util.*;

public class Car implements Comparable{
	String mark;
	String color;
	int length;
	
	Car(String m, String c, int l){
		mark = m;
		color = c;
		length = l;
	}
	
	public int compareTo(Object arg0){
		Car c = (Car) arg0;
		int rez = color.compareTo(c.color);
		if(rez != 0){
			return rez;
		}	
		rez = mark.compareTo(c.mark);
		if(rez != 0){
			return rez;
		}
		rez = length - c.length;
		if(rez != 0){
			return -rez / Math.abs(rez);
		}	
		return 0;
	}
	
	public static void main(String args[]){
		
		List <Car> cars = new ArrayList<>();
		cars.add(new Car("Opel", "black", 3));
		cars.add(new Car("Geep", "black", 5));
		cars.add(new Car("Geep", "black", 4));
	    cars.add(new Car("Toyota", "gray", 4));
	    cars.add(new Car("Toyota", "gray", 3));
		cars.add(new Car("Jaguar", "red", 4));
	    cars.add(new Car("Lexus", "red", 5));
	   
	    Collections.sort(cars);
	    
	    for(Car car: cars){
			System.out.println("mark: "+ car.mark + "; color: " + car.color + "; length: " + car.length);
		}
	    
	}
}
