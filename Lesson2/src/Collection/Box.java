package Collection;

import java.util.*;

public class Box implements Collection{
	final String c1 = "Bad candidate";
	final String c2 = "Good candidate";
	static List <String> candidates = new ArrayList<>();
	
	@Override
	public boolean add(Object e) {
		String s = (String) e; 
		if(!s.equalsIgnoreCase(c1)){
			if(s.equalsIgnoreCase(c2)){
				candidates.add(s);
				candidates.add(s);
			}else{
				candidates.add(s);
			}
		}
		return true;
	}

	@Override
	public boolean addAll(Collection c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void clear() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean contains(Object o) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean containsAll(Collection c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Iterator iterator() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean remove(Object o) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean removeAll(Collection c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean retainAll(Collection c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Object[] toArray() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object[] toArray(Object[] a) {
		// TODO Auto-generated method stub
		return null;
	}	
	
	public static void main(String args[]){
		Box b = new Box();
		String s = "candidate";
		b.add(s);
		String s2 = "Bad candidate";
		b.add(s2);
		String s3 = "Good candidate";
		b.add(s3);
		
		for(String o: candidates){
			System.out.println(o);
		}
	}
}
