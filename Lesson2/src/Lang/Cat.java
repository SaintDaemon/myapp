package Lang;

import java.util.Arrays;

public class Cat {
	private int[] rgbColor = new int[3];
	private int age;
	
	Cat(){
		rgbColor[0]=100;
		rgbColor[1]=100;
		rgbColor[2]=100;
		age=1;
	}
	
	Cat(int r, int g, int b, int age){
		rgbColor[0]=r;
		rgbColor[1]=g;
		rgbColor[2]=b;
		this.age=age;
	}
	
	public boolean equals(Object othercat){
		if (this == othercat) {
		    return true;
		}
		if (othercat instanceof Cat) {
			Cat cat = (Cat) othercat;
			if(this.age == cat.age && Arrays.equals(this.rgbColor, cat.rgbColor)){
				return true;	
			}
		}
		return false;
	}
	
	public String toString(){
		return "color: " + String.valueOf(rgbColor[0]) + ", " + String.valueOf(rgbColor[1]) + ", " + String.valueOf(rgbColor[2]) + "; age: " + String.valueOf(age) + ";";
	}
	
	public int hashCode(){
		return this.toString().hashCode();
	}
	
	public static void main(String args[]){
		
		Cat c1 = new Cat(100, 100, 70, 3);
		Cat c2 = new Cat();
		Cat c3 = new Cat();
		System.out.println(c1.equals(c2));
		System.out.println(c2.equals(c3));
		System.out.println(c1.hashCode());
		System.out.println(c2.hashCode());
		System.out.println(c3.hashCode());
	}
}
