package Lang;

class MyException extends Exception{
	private int value;
	
	MyException(int v){
		value = v;
	}
	
	public String toString(){
		return "MyExcepton [" + value + "]";
	}
}

class MyRuntimeException extends Exception{
private int value;
	
	MyRuntimeException(int v){
		value = v;
	}
	
	public String toString(){
		return "MyRuntimeException [" + value + "]";
	}
	
}

public class DemoException {
	
	static void exc1() throws MyException{
		try{
			throw new MyException(1);
		}catch(MyException e){
			System.out.println("Caught " + e);
		}
	}
	
	static void exc2() {
		try{
			throw new MyRuntimeException(1);
		}catch(MyRuntimeException e){
			System.out.println("Caught " + e);
		}
	}
	
	public static void main(String args[]) throws MyException, MyRuntimeException{
		exc1();
		exc2();
	}
}
