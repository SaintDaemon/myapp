package Lang;

import java.io.*;

public class Case {
	
	static String getString() throws IOException{
		BufferedReader br1 = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter string: ");
		String s = br1.readLine();
		return s;
	}
 
	static String changeCase(String s){
		Character c = s.charAt(0);
		if(Character.isLowerCase(c)){
			s = Character.toUpperCase(c) + s.substring(1);
		}else{
			s = Character.toLowerCase(c) + s.substring(1);
		}
		return s;
	}

	public static void main(String args[]) throws IOException{
		System.out.println(changeCase(getString()));
	}
}
