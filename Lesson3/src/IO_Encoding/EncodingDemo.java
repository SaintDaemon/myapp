package IO_Encoding;

import java.io.*;

// class that implements recoding
public class EncodingDemo {

	// method that implements recoding
	static void changeEncoding() throws IOException {
		Encoding e = new Encoding();
		for (int i = 1; i <= e.getEncodingsListLength(); i++) {
			System.out.println((i) + ") " + e.getEncoding(i));
		}
		int num[] = e.getTwoNumberEncoding();
		System.out.println("Enter file address");
		String s = e.getString();
		Reader r = null;
		Writer w = null;
		int i = 0;
		File f = new File(s);
		int b[] = new int[(int) f.length()];
		try {
			r = new InputStreamReader(new FileInputStream(s),
					e.getEncoding(num[0]));
			while (i < 24) {
				b[i] = r.read();
				i++;
			}
		} catch (IOException ex) {
			System.out.println("IOException" + ex);
		} finally {
			if (r != null) {
				r.close();
			}
			try {
				w = new OutputStreamWriter(new FileOutputStream(s),
						e.getEncoding(num[1]));
				for (int a = 0; a < i; a++) {
					w.write(b[a]);
				}
			} catch (IOException ex) {
				System.out.println("IOException" + ex);
			} finally {
				if (w != null) {
					w.close();
				}
			}
			System.out.println("Recoding completed");
		}
	}

	public static void main(String args[]) throws IOException {
		changeEncoding();
	}
}
