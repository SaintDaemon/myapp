package IO_Encoding;

import java.io.*;

// class that contains methods needed to prepare recoding
public class Encoding {
	private String[] encodingsList = { "ASCII", "ISO8859_1", "ISO8859_2",
			"ISO8859_3", "ISO8859_4", "ISO8859_5", "ISO8859_6", "ISO8859_7",
			"ISO8859_8", "ISO8859_9", "Big5", "Cp037", "Cp1006", "Cp1025",
			"Cp1026", "Cp1046", "Cp1097", "Cp1098", "Cp1112", "Cp1122",
			"Cp1123", "Cp1124", "Cp1250", "Cp1251", "Cp1252", "Cp1253",
			"Cp1254", "Cp1255", "Cp1256", "Cp1257", "Cp1258", "Cp1381",
			"Cp1383", "Cp273", "Cp277", "Cp278", "Cp280", "Cp284", "Cp285",
			"Cp297", "Cp33722", "Cp420", "Cp424", "Cp437", "Cp500", "Cp737",
			"Cp775", "Cp838", "Cp850", "Cp852", "Cp855", "Cp857", "Cp860",
			"Cp861", "Cp862", "Cp863", "Cp864", "Cp865", "Cp866", "Cp868",
			"Cp869", "Cp870", "Cp871", "Cp874", "Cp875", "Cp918", "Cp921",
			"Cp922", "Cp930", "Cp933", "Cp935", "Cp937", "Cp939", "Cp942",
			"Cp948", "Cp949", "Cp950", "Cp964", "Cp970", "EUC_CN", "EUC_JP",
			"EUC_KR", "EUC_TW", "GBK", "ISO2022CN_CNS", "ISO2022JP",
			"ISO2022KR", "JIS0201", "JIS0208", "JIS0212", "KOI8_R", "MS874",
			"MacArabic", "MacCentralEurope", "MacCroatian", "MacCyrillic",
			"MacDingbat", "MacGreek", "MacHebrew", "MacIceland", "MacRoman",
			"MacRomania", "MacSymbol", "MacThai", "MacTurkish", "MacUkraine",
			"SJIS", "UTF8", "UTF-16", "UnicodeBig", "UnicodeLittle" };

	// method that returns the encoding of the desired number
	String getEncoding(int i) {
		String s = encodingsList[i - 1];
		return s;
	}

	// method that returns the encodingsList length
	int getEncodingsListLength() {
		int l = encodingsList.length;
		return l;
	}

	// method that reads a string from the console
	String getString() throws IOException {
		BufferedReader br1 = new BufferedReader(
				new InputStreamReader(System.in));
		String s = br1.readLine();
		return s;
	}

	// method that reads and returns two numbers of possible encodings
	int[] getTwoNumberEncoding() throws NumberFormatException, IOException {
		int[] twoEnc = new int[2];
		twoEnc[0] = 0;
		twoEnc[1] = 0;
		System.out.println("Enter the number of desired coding input");
		do {
			try {
				twoEnc[0] = Integer.parseInt(getString());
				if ((twoEnc[0] < 1) || (twoEnc[0] > 111)) {
					twoEnc[0] = 0;
					System.out.println("Enter the correct number!");
				}
			} catch (NumberFormatException e) {
				System.out.println("Enter the number!");
			}
		} while (twoEnc[0] == 0);

		System.out.println("Enter the number of desired coding output");
		do {
			try {
				twoEnc[1] = Integer.parseInt(getString());
				if ((twoEnc[1] < 1) || (twoEnc[1] > 111)) {
					twoEnc[1] = 0;
					System.out.println("Enter the correct number!");
				}
			} catch (NumberFormatException e) {
				System.out.println("Enter the number!");
			}
		} while (twoEnc[1] == 0);

		return twoEnc;
	}
}
