package IO_Copy;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;

// class that contains methods for copying
public class Copy {
	
	// method that reads a string from the console
	String getString() throws IOException {
		BufferedReader br1 = new BufferedReader(new InputStreamReader(System.in));
		String s = br1.readLine();
		return s;
	}
	
	// method that copies the file without buffering
	void Copy() throws IOException{
		System.out.println("Enter file address: ");
		String s = getString();
		System.out.println("Enter file address: ");
		String s2 = getString();
		
		InputStream f = null;
		OutputStream f2 = null;
		Date d = new Date();
		long timeB = 0;
		long timeE = 0;
		String time;
		
		try{
			f = new FileInputStream(s);
			f2 = new FileOutputStream(s2);
			int i;
			timeB = d.getTime();
			System.out.println("Please wait...");
			do{
				i = f.read();
				if(i != -1){
					f2.write(i);
				}
			} while(i != -1);
			f.close();
			f2.close();
		} catch (IOException e){
			System.out.println("I/O Exception " + e);
		} finally{
			if(f != null){
				f.close();
			}
			if(f2 != null){
				f2.close();
			}
		}
		d = new Date();
		timeE = d.getTime();
		System.out.println("Copying is completed ");
		time = new SimpleDateFormat("mm:ss").format(timeE - timeB);
		System.out.println("Copying time = " + time);
	}
	
	// method that copies the file with buffering
	void BufferedCopy() throws IOException{
		System.out.println("Enter file address: ");
		String s = getString();
		System.out.println("Enter file address: ");
		String s2 = getString();
		
		Date d = new Date();
		long timeB = 0;
		long timeE = 0;
		String time;
		BufferedInputStream b = null;
		BufferedOutputStream b2 = null;
		
		try{
			b = new BufferedInputStream(new FileInputStream(s));
			b2 = new BufferedOutputStream(new FileOutputStream(s2));
			int i;
			timeB = d.getTime();
			System.out.println("Please wait...");
			do{
				i = b.read();
				if(i != -1){
					b2.write(i);
				}
			} while(i != -1);
			d = new Date();
			timeE = d.getTime();
			System.out.println("Copying is completed ");
			time = new SimpleDateFormat("mm:ss").format(timeE - timeB);
			System.out.println("Copying time = " + time);
		} catch (IOException e){
			System.out.println("I/O Exception " + e);
		} finally{
			if(b != null){
				b.close();
			}
			if(b2 != null){
				b2.close();
			}
		}
		
	}	
}
