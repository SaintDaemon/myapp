package IO_Copy;

import java.io.IOException;

// class that implements copy
public class CopyDemo {

	// class that implements the copy with the choice of copying
	static void implementationCopying() throws IOException {
		Copy c = new Copy();
		String s;
		do {
			System.out.println("Do you wont buffered copy?(Yes/No)");
			s = c.getString();
			if (s.equalsIgnoreCase("Yes")) {
				c.BufferedCopy();
			} else {
				if (s.equalsIgnoreCase("No")) {
					c.Copy();
				} else {
					System.out.println("Answer must be \"Yes\" or \"No\"!");
				}
			}
		} while (!s.equalsIgnoreCase("Yes") && !s.equalsIgnoreCase("No"));
	}

	public static void main(String args[]) throws IOException {
		implementationCopying();
	}
}
