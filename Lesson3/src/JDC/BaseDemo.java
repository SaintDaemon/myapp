package JDC;

import java.io.*;
import java.sql.*;

// class that implements the work with database
public class BaseDemo {

	// method that implements the work with database
	static void workingWithDatabases() throws IOException, SQLException,
			InstantiationException, IllegalAccessException,
			ClassNotFoundException {
		Base b = new Base();
		String s2;
		do {
			System.out.println("Possible options: \n 1) Add table (enter :) )\n 2) Make SQLUpdate (enter '-' )\n 3) Make select query (enter ^__^ )\n 4) Create database (enter O_o )\n 5) Exit (enter XD )");
			System.out.println("Select the next action: ");
			s2 = b.getString();
			switch (s2) {
			case ":)":
				System.out.println("<--Adding a table to the database...");
				Connection c = b.connect();
				b.addTable(c);
				System.out
						.println("...adding a table to the database is completed)");
				break;

			case "'-'":
				System.out.println("<--Creation of a database query...");
				Connection c2 = b.connect();
				b.makeSqlUpdate(c2);
				System.out
						.println("...creation of a database query is completed)");
				break;

			case "^__^":
				System.out
						.println("<--Creation a select query to the database...");
				Connection c3 = b.connect();
				b.select(c3);
				System.out.println("...creation a select query to the database is completed)");
				break;

			case "O_o":
				System.out.println("<--Database creation-->");
				b.createDBase();
				System.out.println("...database creation is completed)");
				break;
			case "XD":
				System.out.println("Goodbye)");
				break;
			}

		} while (!s2.equals("XD"));
	}

	public static void main(String arg[]) throws InstantiationException,IllegalAccessException, ClassNotFoundException, IOException,SQLException {
		workingWithDatabases();
	}
}

