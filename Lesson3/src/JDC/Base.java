package JDC;

import java.io.*;
import java.sql.*;
import java.util.Scanner;

// class that contains methods to create the database, connection, disconnection and work with it
public class Base {
	
	// method that reads a string from the console
	String getString() throws IOException {
		BufferedReader br1 = new BufferedReader(
				new InputStreamReader(System.in));
		String s = br1.readLine();
		return s;
	}
		
	// method that creates a database
	void createDBase() throws IOException, SQLException{
		System.out.println("Enter tour data base name: ");
		String name = getString();
		
		Connection conn = null;
		Statement statement = null;
		try{
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			System.out.println("Driver loading success!");
			
			conn= DriverManager.getConnection("jdbc:mysql://localhost/mysql", "root", "");
			statement = conn.createStatement();
			statement.executeUpdate("CREATE DATABASE " + name + " CHARACTER SET utf8 COLLATE utf8_general_ci");
		} catch(Exception e){
			System.out.println("Exception! " + e);
		} finally {
			conn.close();
		}
		
		
	}
	
	// method that connects to database
	Connection connect() throws IOException, SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException{
		Connection conn = null;
		try{
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			System.out.println("Driver loading success!");
		
			System.out.println("Enter your host: ");
			String host = getString();
			System.out.println("Enter your data base name: ");
			String dbName = getString();
			System.out.println("Enter your user name: ");
			String userName = getString();
			System.out.println("Enter your password: ");
			String password = getString();
			
			try{
				conn = DriverManager.getConnection(host + dbName, userName, password);
				System.out.println("Connected...");
				
			} catch (SQLException e){
				System.out.println("SQLException! " + e);
			}
		} catch (ClassNotFoundException e) {
			System.out.println("ClassNotFoundException! " + e);
		}	
		return conn;
	}
	
	// method that performs disconnected from database
	void discconect(Connection conn) throws SQLException{
		try{
			conn.close();
			System.out.println("...Disconnected");
		} catch(SQLException e) {
			System.out.println("SQLException! " + e);
		}
	}
	
	// method that adds to database a table with columns
	void addTable(Connection conn) throws InstantiationException, IllegalAccessException, ClassNotFoundException, IOException, SQLException{
		Statement  stat = null;
		System.out.println("Enter table name: ");
		String table = getString();
		System.out.println("Enter your fields with properties: ");
		String fields = getString();
		try{
			stat = conn.createStatement();
			stat.executeUpdate("CREATE TABLE `" + table + "` (" + fields + ") ENGINE=InnoDB DEFAULT CHARSET=utf8;");
			System.out.println("Table is created");
		} catch(SQLException e) {
			System.out.println("SQLException! " + e);
		} finally {
			discconect(conn);
		}
	}
	
	// method that makes sql database query
	void makeSqlUpdate(Connection conn) throws InstantiationException, IllegalAccessException, ClassNotFoundException, IOException, SQLException{
		Statement  stat = null;
		System.out.println("Enter your query: ");
		String query = getString();
		try{
			stat = conn.createStatement();
			int i = stat.executeUpdate(query);
			System.out.println("Query executed \nNumber of edited records: " + i);
		} catch(SQLException e) {
			System.out.println("SQLException! " + e);
		} finally {
			discconect(conn);
		}
	}
	
	// method that makes  select query to database 
	void select(Connection conn) throws InstantiationException, IllegalAccessException, ClassNotFoundException, IOException, SQLException{
		Statement  stat = null;
		try{
			stat = conn.createStatement();
			System.out.println("Complete the query: \n Select * from ");
			String t = getString();
			ResultSet rez = stat.executeQuery("Select * from " + t);
			printResults(rez);
		} catch (SQLException e){
			System.out.println("SQLException! " + e);
		} finally {
			discconect(conn);
		}
	}
	
	// method, which adds data from the database into an array and displays them in an acceptable form
	void printResults(ResultSet rs) throws SQLException, IOException {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter number of columns: ");
		int num = sc.nextInt();
		String columnsNames[] = new String[num];
		int num2 = 0;
		while(rs.next()){
			num2++;
		}
		rs.first();
		String baseTable[][] = new String[num2][num];
		int a = 0;
		for(int i = 0; i < num; i++){
			System.out.println((i+1) + ") Enter column name: ");
			columnsNames[i] = getString();
			do{
				baseTable[a][i] = rs.getString(columnsNames[i]);
				a++;
			} while(rs.next());
			rs.first();
			a = 0;
		}
		System.out.println("_____________________________");
		for(int i = 0; i < num; i++){
			System.out.print(columnsNames[i] + "    ");
		}
		System.out.println();
		for(int j = 0; j < num2; j++){
			for(int i = 0; i < num; i++){
				System.out.print(baseTable[j][i] + "    ");
			}
			System.out.println();
		}
		System.out.println("_____________________________");
	}
	
}
		
