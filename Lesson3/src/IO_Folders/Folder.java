package IO_Folders;

import java.io.*;

// class that contains methods to create, rename and delete folders
public class Folder {

	// method that reads a string from the console
	String getString() throws IOException {
		BufferedReader br1 = new BufferedReader(
				new InputStreamReader(System.in));
		String s = br1.readLine();
		return s;
	}

	// method that implements create a folder
	void createFolder() throws IOException {
		System.out.println("Enter folder address for creating: ");
		String s = getString();
		File f = new File(s);
		if (f.mkdir()) {
			System.out.println("Folder created");
		} else {
			System.out.println("Folder not created!");
		}
	}

	// method that implements rename a folder
	void renameFolder() throws IOException {
		System.out.println("Enter folder address for renaming: ");
		String s = getString();
		File f = new File(s);
		System.out.println("Enter new folder address: ");
		String s2 = getString();
		File f2 = new File(s2);
		if (f.renameTo(f2)) {
			System.out.println("Folder renamed");
		} else {
			System.out.println("Folder not renamed!");
		}
	}

	// method that implements delete a folder
	void deleteFolder() throws IOException {
		System.out.println("Enter folder address for removing: ");
		String s = getString();
		File f = new File(s);
		if (f.delete()) {
			System.out.println("Folder deleted");
		} else {
			System.out.println("Folder not deleted!");
		}
	}

}
