package IO_Folders;

import java.io.*;

// class that implements work with folders
public class FoldersDemo {

	// method that implements work with folders
	static void workingWithFolders() throws IOException {
		Folder f = new Folder();
		char c;
		System.out.println("Enter \"A\" to creat a folder");
		System.out.println("Enter \"B\" to rename a folder");
		System.out.println("Enter \"C\" to delete a folder");
		System.out.println("Enter \"E\" to exit");
		do {
			System.out.println("What do you want?");
			BufferedReader br1 = new BufferedReader(new InputStreamReader(
					System.in));
			c = (char) br1.read();
			switch (c) {
			case 'A':
				f.createFolder();
				break;
			case 'B':
				f.renameFolder();
				break;
			case 'C':
				f.deleteFolder();
				break;
			case 'E':
				System.out.println("Working with folders completed");
				break;
			default:
				System.out.println("You must enter \"A\" or \"B\" or \"C\"!!!");
				break;
			}

		} while (c != 'E');
	}

	public static void main(String args[]) throws IOException {
		workingWithFolders();
	}
}
