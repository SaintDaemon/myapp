<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
	<head>
		<script src="http://code.jquery.com/jquery-latest.js"></script>
		<script type="text/javascript" src="http://jzaefferer.github.com/jquery-validation/jquery.validate.js"></script>
		<script>
			$(document).ready(function(){
				$("#form").validate({
					rules: {
					    password: {
					      	required: true,
					      	minlength: 6,
					      	maxlength: 30
					    }
					},
					messages: {
						login: "	Please enter your login",
						password: {
							required: "	Please enter your password",
							minlength: "	Too short",
							maxlength: "	Too long"
						},
						name: "	Please enter your name"
					}
				});
			});
		</script>
		
		<style>
			.error{
				color: red
			}
		</style>
	</head>
	
	<body>
		<form id="form" action="saveUser.html">
		    Login <input type="text" value="${user.login}" name="login" class="required"><br>
		    Password <input type="text" value="${user.password}" name="password"><br>
		    Name <input type="text" value="${user.name}" name="name" class="required"><br>
		    <input type="hidden" name="id" value="${user.id}">
		    <input type="submit" value="Save">
		    
		    <p><a href="index.html">Home</a></p>
		</form>
	</body>	
</html>