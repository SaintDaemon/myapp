package com.geekhub.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.geekhub.beans.User;
import com.geekhub.services.UserDAO;

@Controller
public class UserController {
	
	@Autowired UserDAO userDAO;
	
	@RequestMapping(value="listUsers.html")
	public String list(ModelMap map) {
		map.put("users",userDAO.getUserList());
		return "users";
	}
	
	@RequestMapping(value="loadUser.html")
	public String load(@RequestParam(value="id", required=false) Integer id, ModelMap map) {
		User user = id == null ? new User() : userDAO.getUserById(id);
		map.put("user", user);
		return "user";
	}
	
	@RequestMapping(value="deleteUser.html")
	public String delete(@RequestParam(value="id", required=true) Integer id) {
		userDAO.deleteUser(id);
		return "redirect:listUsers.html";
	}
	
	@RequestMapping(value="saveUser.html")
	public String save(User user) {
		userDAO.saveUser(user);
		return "redirect:listUsers.html";
	}
	
	@ResponseBody
	@RequestMapping(value="verify.html")
	public String verify(@RequestParam(value="name", required=true) String name) {
		User user = userDAO.getUserByName(name);
		boolean status = true;
		if(user != null){
			status = false;
		}
		return status + "";
	}
	
}
