package com.geekhub.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.geekhub.services.UserDAO;

@Controller
public class BaseController {
	
	@Autowired UserDAO userDAO;
	
	@RequestMapping(value="index.html")
	public String index(ModelMap map) {
		map.put("users", userDAO.getUserList().size());
		return "index";
	}
}
