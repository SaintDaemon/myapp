package com.geekhub.services;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.geekhub.beans.User;

@Repository
@Transactional
public class UserDAOImpl implements UserDAO {
	
	@Autowired SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	
	@Override
	public List<User> getUserList() {
		return sessionFactory.getCurrentSession().createQuery("from User").list();
	}

	@Override
	public User getUserById(Integer id) {
		return (User) sessionFactory.getCurrentSession().get(User.class, id);
	}

	@Override
	public void deleteUser(Integer id) {
		User user = getUserById(id);
		if(null != user)
			sessionFactory.getCurrentSession().delete(user);
	}

	@Override
	public void saveUser(User user) {
		sessionFactory.getCurrentSession().saveOrUpdate(user);
	}

	@Override
	public User getUserByName(String name) {
		Session session = sessionFactory.getCurrentSession();
		String hql = "select user from User user where user.name = :userName";
		try{
			User user = (User) session.createQuery(hql).setString("userName", name).uniqueResult();
			return user;
		} catch (NullPointerException e){
			System.out.println(e);
		}
		return null;
	}

}
