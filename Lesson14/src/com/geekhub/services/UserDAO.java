package com.geekhub.services;

import java.util.List;

import com.geekhub.beans.User;

public interface UserDAO {

	public List<User> getUserList();
	
	public User getUserById(Integer id);
	
	public void deleteUser(Integer id);
	
	public void saveUser(User user);

	public User getUserByName(String name);

}
