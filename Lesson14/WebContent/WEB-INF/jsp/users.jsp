<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<table border="1">
    <tr>
        <th>Id</th>
        <th>Login</th>
        <th>Password</th>
        <th>Name</th>
        <th></th>
        <th></th>
    </tr>

<c:forEach items="${users}" var="user">
    <tr>
        <td>${user.id}</td>
        <td>${user.login}</td>
        <td>${user.password}</td>
        <td>${user.name}</td>
        <td><a href="loadUser.html?id=${user.id}">Edit</a></td>
        <td style="color: red;"><a href="deleteUser.html?id=${user.id}">Delete</a></td>

    </tr>
</c:forEach>

</table>
<a href="loadUser.html">Create New User</a>
<p><a href="index.html">Home</a></p>
