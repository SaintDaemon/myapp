<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
	<script src="http://code.jquery.com/jquery-latest.js"></script>
	<script type="text/javascript" src="http://jzaefferer.github.com/jquery-validation/jquery.validate.js"></script>
	<script>
		function go(){
			$('#submit').attr('disabled','disabled');
			$("#loading").removeClass("l");
			var userName = document.getElementsByName("name")[0].value;
			$.ajax({
				url: 'verify.html',
				type: 'GET',
				data: {name: userName},
				dataType: "text",
				success: function(msg){
					if("true" == msg){
						$("#submit").removeAttr("disabled");
						$("#loading").addClass("l");
						$("#msg").removeClass("error");
						$("#msg").addClass("success");
						$("#msg").empty();
						$("#msg").append("Ok");
					} else {
						$("#loading").addClass("l");
						$("#msg").removeClass("success");
						$("#msg").addClass("error");
						$("#msg").empty();
						$("#msg").append("Not unique");
					}
				}
			});
		}
	</script>
	
	<style>
		.l{
			visibility: hidden;
		}
		
		.error{
			color: red;
		}
		
		.success{
			color:green;
		}
	</style>

</head>

<body>
	<form action="saveUser.html">
		<p>Login <input type="text" value="${user.login}" name="login"></p>
		<p>Password <input type="text" value="${user.password}" name="password"></p>
		Name <input type="text" id="name" value="${user.name}" name="name" onchange="go()">
		<span id="msg"></span>
		<input type="hidden" name="id" value="${user.id}">
		<p><input id="submit" type="submit" value="Save"></p>
		<p><a href="index.html">Home</a></p>
		<img class="l" id="loading" src="${pageContext.servletContext.contextPath}/loading.gif" alt="loading">
	</form>
</body>
</html>