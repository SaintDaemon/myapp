package Net_Socket;

import java.io.*;
import java.net.Socket;

public class SimpleClient {
	private static final int DEFAULT_PORT = 100;
	
	// method that reads a string from the console
	static String getString() throws IOException {
		BufferedReader br1 = new BufferedReader(new InputStreamReader(System.in));
		String s = br1.readLine();
		return s;
	}

	// method that starts the client
	static void startClient() throws IOException {
		String ex = "";
		Socket client = new Socket("127.0.0.1", DEFAULT_PORT);
		do {
			System.out.println("Enter string: ");
			String defaultString = getString();
			if (!defaultString.equals("")) {
				DataOutputStream outToServer = new DataOutputStream(client.getOutputStream());
				outToServer.writeBytes(defaultString + "\n");
				BufferedReader in = new BufferedReader(new InputStreamReader(client.getInputStream()));
				char modifiedString = (char) in.read();
				System.out.println("From server: " + modifiedString);
				System.out.println("Enter E to exit");
				ex = getString();
			}
		} while (!ex.equals("E"));
		System.out.println("Connection completed");
		client.close();
	}

	public static void main(String args[]) throws IOException {
		startClient();
	}
}