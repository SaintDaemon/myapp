package Net_Socket;

import java.io.*;
import java.net.*;

public class SimpleServer {

	private static final int DEFAULT_PORT = 100;

	// method that starts the server
	static void startServer() throws IOException {
		String clientString;
		ServerSocket server = new ServerSocket(DEFAULT_PORT);
		while (true) {
			Socket conn = server.accept();
			System.out.println("Connected");
			BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			DataOutputStream outToClient = new DataOutputStream(conn.getOutputStream());
			try {
				while ((clientString = in.readLine()) != null) {
					System.out.println("Client string : " + clientString);
					String modifiedString = clientString.substring(0, 1);
					outToClient.writeBytes(modifiedString + "\n");
				}
			} catch (IOException e) {
				System.out.println(e);
			} finally {
				server.close();
			}
		}
	}

	public static void main(String args[]) throws IOException {
		startServer();
	}
}