package Thread;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Date;

public class Search implements Runnable {
	private String address;
	private String patern;
	
	public Search() {}

	Search(String address, String patern){
		this.address = address;
		this.patern = patern;
	}
	
	// method that reads a string from the console
	static String getString() throws IOException {
		BufferedReader br1 = new BufferedReader(new InputStreamReader(System.in));
		String s = br1.readLine();
	return s;
	}
	
	
	public void run() {
		String[] mass = null;
		File file = new File(address);
		mass = file.list();
		if(mass != null){
			for(int i = 0; i < mass.length; i++){
				File f2 = new File(address + "/" + mass[i]);
				if(f2.isDirectory()){
					new Thread(new Search(address + "/" + mass[i], patern)).start();
				} else{
					if(mass[i].endsWith(patern.substring(1))){
						System.out.println(mass[i]);
					}	
				}
			}
		}
	}
	
	public static void main(String args[]) throws IOException{
		Search s = new Search();
		System.out.println("Enter folder address: ");
		s.address = getString();
		System.out.println("Enter patern: ");
		s.patern = getString();
		new Thread(s).start();	
	}
}