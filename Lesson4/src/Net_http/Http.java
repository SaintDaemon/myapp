package Net_http;

import java.util.*;
import java.io.*;
import java.net.*;

public class Http {
	private String url;
	Set<String> list200 = new TreeSet<>();
	Set<String> list300 = new TreeSet<>();
	Set<String> list400 = new TreeSet<>();
	Set<String> listOther = new TreeSet<>();

	// method that reads a string from the console
	static String getString() throws IOException {
		BufferedReader br1 = new BufferedReader(new InputStreamReader(System.in));
		String s = br1.readLine();
		return s;
	}
	
	//method that returns a string containing the html code of the page
	private String readContent() throws IOException {
		String str, str2 = "";
		BufferedReader in = null;
		try {
			URLConnection conn = new URL(url).openConnection();
			in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			while ((str = in.readLine()) != null) {
				str2 += str;
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (in != null) {
				in.close();
			}
		}
		return str2;
	}
	
	// method, that returns a list of all URL
	List<String> getList() throws IOException {
		System.out.println("Enter URL: ");
		url = getString();
		List<String> list = new ArrayList<>();
		String str2 = readContent().replaceAll(" ", "");
		int i = 0;
		while ((i = str2.indexOf("<a")) != -1) {
			str2 = str2.substring(i);
			i = str2.indexOf("href=\"");
			if (i != -1) {
				str2 = str2.substring(i + 6);
				i = str2.indexOf("\"");
				list.add(str2.substring(0, i));
				str2 = str2.substring(i);
			}
		}
		return list;
	}

	// method, that allocates all URL into three groups
	void distribution(List<String> list) throws IOException {
		HttpURLConnection httpConn;
		for (Object l : list) {
			String s = (String) l;
			if (s.startsWith("http")) {
				httpConn = (HttpURLConnection) new URL(s).openConnection();
				httpConn.setRequestMethod("HEAD");
				int i = httpConn.getResponseCode();
				if (i >= 200 && i < 300) {
					list200.add(s);
				} else if (i >= 300 && i < 400) {
					list300.add(s);
				} else if (i >= 400 && i < 500) {
					list400.add(s);
				}
			} else {
				if (s.startsWith("/")) {
					httpConn = (HttpURLConnection) new URL(url + l).openConnection();
					httpConn.setRequestMethod("HEAD");
					int i = httpConn.getResponseCode();
					if (i >= 200 && i < 300) {
						list200.add(s);
					} else if (i >= 300 && i < 400) {
						list300.add(s);
					} else if (i >= 400 && i < 500) {
						list400.add(s);
					}
				}
			}
		}
	}

}