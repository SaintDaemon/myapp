package com.geekhub.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.web.util.NestedServletException;

import com.geekhub.beans.Entity;
import com.geekhub.beans.Ticket;
import com.geekhub.beans.User;

public class BaseDao extends JdbcTemplate {

	public <T extends Entity> T get(Class<T> clazz, Integer id) {
		return queryForObject("SELECT * FROM " + clazz.getSimpleName() + " WHERE id = ?", new Object[] {id}, new BeanPropertyRowMapper<>(clazz));
	}
	
	public <T extends Entity> List<T> list(Class<T> clazz) {
		return query("SELECT * FROM " + clazz.getSimpleName(), new BeanPropertyRowMapper(clazz));
	}
	
	public <T extends Entity> boolean delete(Class<T> clazz, Integer id) {
		return update("DELETE FROM " + clazz.getSimpleName() + " WHERE id = ?", id) > 0;
	}
	
	public void save(final User user) {
		if (user.isNew()) {
			PreparedStatementCreator psc = new PreparedStatementCreator() {
				@Override
				public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
					PreparedStatement ps = con.prepareStatement("INSERT INTO User (login, password, name) VALUES (?,?,?)");
					ps.setString(1, user.getLogin());
					ps.setString(2, user.getPassword());
					ps.setString(3, user.getName());
					return ps;
				}
			};
			update(psc);
		} else {
			update("UPDATE User SET login = ?, password = ?, name = ? WHERE Id = ?", user.getLogin(), user.getPassword(), user.getName(), user.getId());
		}
	}
	
	public void save(final Ticket ticket) {
		if (ticket.isNew()) {
			PreparedStatementCreator psc = new PreparedStatementCreator() {
				@Override
				public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
					PreparedStatement ps = con.prepareStatement("INSERT INTO Ticket (title, description, owner, status, priority) VALUES (?,?,?,?,?)");
					ps.setString(1, ticket.getTitle());
					ps.setString(2, ticket.getDescription());
					ps.setString(3, String.valueOf(ticket.getOwner()));
					ps.setString(4, ticket.getStatus().name());
					ps.setString(5, ticket.getPriority().name());
					return ps;
				}
			};
			update(psc);
		} else {
			update("UPDATE Ticket SET title = ?, description = ?, owner = ?, status = ?, priority = ? WHERE Id = ?", ticket.getTitle(), ticket.getDescription(), ticket.getOwner(), ticket.getStatus().name(), ticket.getPriority().name(), ticket.getId());
		}
	}
	
}