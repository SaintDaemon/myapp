package com.geekhub.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.geekhub.beans.Ticket;
import com.geekhub.beans.User;
import com.geekhub.services.BaseDao;

@Controller
public class TicketController {

	@Autowired BaseDao dao;
	
	@RequestMapping(value="listTickets.html")
	public String list(ModelMap map) {
		map.put("tickets", dao.list(Ticket.class));
		return "tickets";
	}
	
	@RequestMapping(value="/loadTicket.html")
	public String load(@RequestParam(value="id", required=false) Integer id, ModelMap map) {
		Ticket ticket = id == null ? new Ticket() : dao.get(Ticket.class, id);
		map.put("ticket", ticket);
		map.put("users", dao.list(User.class));
		return "ticket";
	}
	
	@RequestMapping(value="/saveTicket.html")
	public String save(Ticket ticket) {
		dao.save(ticket);
		return "redirect:listTickets.html";
	}

}
