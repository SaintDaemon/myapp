package BeanRepresenter;

public class Car {
	private String mark;
	private int year;
	
	public Car(String mark, int year) {
		super();
		this.mark = mark;
		this.year = year;
	}
	
	public String getMark() {
		return mark;
	}
	public void setMark(String mark) {
		this.mark = mark;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
}
