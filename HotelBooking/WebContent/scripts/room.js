function go(){
	$('#submit').attr('disabled','disabled');
	$("#loading").removeClass("l");
	var number = document.getElementsByName("number")[0].value;
	$.ajax({
		url: '/HotelBooking/admin/verifyRoomNumber.action',
		type: 'GET',
		data: {number: number},
		dataType: "text",
		success: function(msg){
			if("true" == msg){
				$("#submit").removeAttr("disabled");
				$("#loading").addClass("l");
				$("#msg").removeClass("error");
				$("#msg").addClass("success");
				$("#msg").empty();
				$("#msg").append("Ok");
			} else {
				if("false" == msg){
					$("#loading").addClass("l");
					$("#msg").removeClass("success");
					$("#msg").addClass("error");
					$("#msg").empty();
					$("#msg").append("Enter Number");
				} else {
					$("#loading").addClass("l");
					$("#msg").empty();
					$("#msg").removeClass("success");
					$("#msg").addClass("error");
				}
			}
		}
	});
}

function go2(){
	$('#submit').attr('disabled','disabled');
	$("#loading").removeClass("l");
	var price = document.getElementsByName("price")[0].value;
	$.ajax({
		url: '/HotelBooking/admin/verifyRoomPrice.action',
		type: 'GET',
		data: {price: price},
		dataType: "text",
		success: function(msg){
			if("true" == msg){
				$("#submit").removeAttr("disabled");
				$("#loading").addClass("l");
				$("#msg2").removeClass("error");
				$("#msg2").addClass("success");
				$("#msg2").empty();
				$("#msg2").append("Ok");
			} else {
				if("false" == msg){
					$("#loading").addClass("l");
					$("#msg2").removeClass("success");
					$("#msg2").addClass("error");
					$("#msg2").empty();
					$("#msg2").append("Enter Number");
				} else {
					$("#loading").addClass("l");
					$("#msg2").empty();
					$("#msg2").removeClass("success");
					$("#msg2").addClass("error");
				}
			}
		}
	});
}

function doSelType(){ 
	 var p = parseInt($("*[name='roomType']")[0].value);
	 document.all['sel'].options[p-1].selected=true;
} 