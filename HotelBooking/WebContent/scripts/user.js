function go(){
	$('#submit').attr('disabled','disabled');
	$("#loading").removeClass("l");
	var username = document.getElementsByName("username")[0].value;
	$.ajax({
		url: '/HotelBooking/verify.action',
		type: 'GET',
		data: {username: username},
		dataType: "text",
		success: function(msg){
			if("true" == msg){
				$("#submit").removeAttr("disabled");
				$("#loading").addClass("l");
				$("#msg").removeClass("error");
				$("#msg").addClass("success");
				$("#msg").empty();
				$("#msg").append("Ok");
			} else {
				if("false" == msg){
					$("#loading").addClass("l");
					$("#msg").removeClass("success");
					$("#msg").addClass("error");
					$("#msg").empty();
					$("#msg").append("Not unique");
				} else {
					$("#loading").addClass("l");
					$("#msg").empty();
				}
			}
		}
	});
}