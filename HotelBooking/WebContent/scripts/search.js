		function date(){
			var date = $("*[name='datepicker']").datepicker( "getDate" );
			var date2 = $("*[name='datepicker2']").datepicker( "getDate" );
			if(date2 == null){
				$("*[name='datepicker2']").datepicker( "option", "minDate", new Date(date) );
				$("*[name='datepicker2']").datepicker( "setDate", new Date(date));
			} else {
				$("*[name='datepicker2']").datepicker( "option", "minDate", new Date(date) );
				if(date2 < date1){
					$("*[name='datepicker2']").datepicker( "setDate", new Date(date));
				}
			}
		}
		
		function date2(){
			var date2 = $("*[name='datepicker2']").datepicker( "getDate" );
			$("*[name='datepicker']").datepicker( "option", "maxDate", new Date(date2) );
		
		}
		
		function setDate(){
			var dateIn = $("*[name='dateIn']")[0].value;
			var dateOut = $("*[name='dateOut']")[0].value;
			
			var date1 = new Date(dateIn);
			var date2 = new Date(dateOut);
			
			$("*[name='datepicker']").datepicker( "setDate", date1);
			$("*[name='datepicker2']").datepicker( "setDate", date2);
		}
		