<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ page session="false"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Hotel Booking</title>
	<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/styles/styles.css"/>
	<script src="${pageContext.servletContext.contextPath}/scripts/jquery-latest.js"></script>
	<script type="text/javascript" src="${pageContext.servletContext.contextPath}/scripts/jquery.validate.js"></script>
	<script src="${pageContext.servletContext.contextPath}/scripts/user.js" type="text/javascript"></script>
	<%@include file="./styles.jsp" %>
</head>

<body>
	
	<%@include file="./headAndLeftPanel.jsp" %>
		
	<div class="rightBlock">
		<h3>You can edit the data</h3>
		
		<form id="form" action="/HotelBooking/user/saveEditedAccount.html">
			<table cellspacing="20">
				<tr>
					<td>Username:</td><td><input type="text" name="username" value="${user.username}" class="required" onchange="go()"></input> <span id="msg"></span></td>
				</tr>
				<tr>
					<td>Password:</td><td><input type="text" name="password"></input></td>
				</tr>
					<td>Name:</td><td><input type="text" name="name" class="required" value="${user.name}"></input></td>
				</tr>
				<tr>
					<td>Email:</td><td><input type="text" name="email" class="required" value="${user.email}"></input></td>
				</tr>
				<tr>
					<td>Telephone:</td><td><input type="text" name="telephone" class="required" value="${user.telephone}"></input></td>
				</tr>
				<tr>
					<td>Country:</td><td><input type="text" name="country" class="required" value="${user.country}"></input></td>
				</tr>
			</table>
			<input type="hidden" name="id" value="${user.id}"></input>
			
			<p><input id="submit" type="submit" value="Save"></input></p>
			
			<div class="img">
				<img class="l" id="loading" src="${pageContext.servletContext.contextPath}/loading.gif" alt="loading">
			</div>
		</form>
	</div>

</body>
</html>