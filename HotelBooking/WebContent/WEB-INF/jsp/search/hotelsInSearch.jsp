<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ page session="false"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/styles/styles.css"/>
	<title>Hotel Booking</title>
	<%@include file="../styles.jsp" %>
</head>

<body>

	<%@include file="../headAndLeftPanel.jsp" %>
		
	<div class="rightBlock">
		<h3>Search results</h3>
		<a href="/HotelBooking/search/sort.action">asc/desc</a>
		<c:forEach var="hotel" items="${hotels}">
			<pre><a href="<c:url value="/search/hotelInSearch.action?id=${hotel.id}"/>" class="list">${hotel.name} (${hotel.city} ${hotel.country}) ${hotel.priorityView}</a></pre>
		</c:forEach>
	</div>
</body>
</html>