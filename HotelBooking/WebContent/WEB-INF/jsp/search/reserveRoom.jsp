<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ page session="false"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Hotel Booking</title>
	<script src="${pageContext.servletContext.contextPath}/scripts/jquery-latest.js"></script>
	<script src="${pageContext.servletContext.contextPath}/scripts/jquery-ui.js"></script>
	<script type="text/javascript" src="${pageContext.servletContext.contextPath}/scripts/jquery.validate.js"></script>
	<script src="${pageContext.servletContext.contextPath}/scripts/search.js" type="text/javascript"></script>
	<link rel="stylesheet" href="${pageContext.servletContext.contextPath}/styles/jquery-ui.css" />
	<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/styles/styles.css"/>
	<%@include file="../styles.jsp" %>
	
	<script>
	    $(document).ready(function(){
	            $("#form").validate({
	            	rules: {
					    email: {
					    	required: true,
					    	email: true
					    }
					},
                    messages: {
                    	email: {
    						required: "	Please enter your email",
    						email: " Please enter correct email"
    					},
                        name: " Please enter name",
                        telephone: "    Please enter telephone"
                    }
	            });
	    });
	
	    $(function() {
	    $( "#datepicker" ).datepicker({
	            minDate: new Date(),
	            maxDate: "+6m",
	            closeText: "Close"
	    });
	    });
	    $(function() {
	    $( "#datepicker2" ).datepicker({
	            minDate: new Date(),
	            maxDate: "+6m"
	    });
	    });
	</script>
	
</head>

<body onload="setDate()">

	<%@include file="../headAndLeftPanel.jsp" %>
	
	<div class="rightBlock">
		<h3>Enter data for reservation</h3>
		<form id="form" action="/HotelBooking/search/finalCheck.action">
			<h4>Name</h4>
			<p><input type="text" name="name" class="required" value="${user.name}"></input>
			<h4>Email</h4>
			<p><input type="text" name="email" class="required" value="${user.email}"></input>
			<h4>Telephone</h4>
			<p><input type="text" name="telephone" class="required" value="${user.telephone}"></input>
			
			<h4><pre>Date of entry:	  Check-out date:</pre></h4>
			<p><pre><input type="text" id="datepicker" name="datepicker" size="9" readonly onchange="date()"/>	     <input type="text" id="datepicker2" name="datepicker2" size="9" readonly onchange="date2()"/></input></pre></p>
			
			<input type="hidden" name="idRoom" value="${room.id}"></input>
			<input type="hidden" name="idUser" value="${user.id}"></input>
			
			<input type="hidden" name="dateIn" value="${dateIn}"></input>
			<input type="hidden" name="dateOut" value="${dateOut}"></input>
			
			<p><input id="submit" type="submit" value="Ok"></input></p>
		</form>
	</div>
	
</body>
</html>