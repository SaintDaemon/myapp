<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ page session="false"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Hotel Booking</title>
	<script src="${pageContext.servletContext.contextPath}/scripts/jquery-latest.js"></script>
	<script type="text/javascript" src="${pageContext.servletContext.contextPath}/scripts/jquery.validate.js"></script>
	<script src="${pageContext.servletContext.contextPath}/scripts/user.js" type="text/javascript"></script>
	<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/styles/styles.css"/>
	<script>
		$(document).ready(function(){
			$("#form").validate({
				rules: {
				    password: {
				      	required: true,
				      	minlength: 6,
				      	maxlength: 30
				    },
				    email: {
				    	required: true,
				    	email: true
				    }
				},
				messages: {
					password: {
						required: "	Please enter your password",
						minlength: "	Too short",
						maxlength: "	Too long"
					},
					email: {
						required: "	Please enter your email",
						email: " Please enter correct email"
					},
					login: "	Please enter your username",
					username: "	Please enter your username",
					name: "	Please enter your name",
					telephone: "	Please enter your telephone",
					country: "	Please enter your country"
				}
			});
		});
	</script>
		
	<%@include file="./styles.jsp" %>
</head>

<body>

	<%@include file="./headAndLeftPanel.jsp" %>
	
	<div class="rightBlock">
		<h3>Please fill the form for registration</h3>
		
		<form id="form" action="saveUser.action">
			
			<table cellspacing="20">
				<tr>
					<td>Username:</td><td><input type="text" name="username" class="required" onchange="go()"></input> <span id="msg"></span></td>
				</tr>
				<tr>
					<td>Password:</td><td><input type="text" name="password"></input></td>
				</tr>
					<td>Name:</td><td><input type="text" name="name" class="required"></input></td>
				</tr>
				<tr>
					<td>Email:</td><td><input type="text" name="email" class="required"></input></td>
				</tr>
				<tr>
					<td>Telephone:</td><td><input type="text" name="telephone" class="required"></input></td>
				</tr>
				<tr>
					<td>Country:</td><td><input type="text" name="country" class="required"></input></td>
				</tr>
				<tr>
			    <td>Status:</td><td><select name="status">
			        <option value="SIMPLE">SIMPLE</option>
			        <option value="ADMIN">ADMIN</option>
			    </select></td>
			    </tr>
			</table>
			
			<p><input id="submit" type="submit" value="Ok"></input></p>
			
			<div class="img">
				<img class="l" id="loading" src="${pageContext.servletContext.contextPath}/loading.gif" alt="loading">
			</div>
		</form>
	</div>
	
</body>
</html>