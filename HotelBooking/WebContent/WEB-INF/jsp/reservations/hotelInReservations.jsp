<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ page session="false"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Hotel Booking</title>
	<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/styles/styles.css"/>
	<%@include file="../styles.jsp" %>
</head>
<body>

	<%@include file="../headAndLeftPanel.jsp" %>
	
	<div class="rightBlock">
		<h3>Your hotel data</h3>
		
		<c:forEach var="image" items="${images}">
			<a href="${pageContext.servletContext.contextPath}/images/${image.content}" rel="iLoad|hotelPhoto"><img src="${pageContext.servletContext.contextPath}/images/${image.content}" title="Open" alt="${image.content}" style="max-width: 200px"/></a>
		</c:forEach>
		
		<pre>Name: ${hotel.name}</pre>
		<pre>Description: ${hotel.description}</pre>
		<pre>Country: ${hotel.country}</pre>
		<pre>City: ${hotel.city}</pre>
		<pre>Priority: ${hotel.priorityView}</pre>
	</div>
	<script type='text/javascript' src='${pageContext.servletContext.contextPath}/images/iLoad.js'></script>
</body>
</html>