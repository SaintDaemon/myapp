	<!-- TopBlocks -->
	<sec:authorize access="isAuthenticated()">
   		<div class="topBlock">
			<div class="topBlock1" align="right"><h2><img src="${pageContext.servletContext.contextPath}/pagesImage/Logged.jpg" alt="Logged in as">${user.username}</h2></div>
	   		<div class="topBlock2"><a href="<c:url value="/user/account.html?id=${user.id}"/>" class="myAccount"/></a></div>
	   		<div class="topBlock3"><a href="<c:url value="/j_spring_security_logout"/>" class="logout"/></a></div>
   		</div>
	</sec:authorize>
	
	<sec:authorize access="isAnonymous()">
		<div class="sign">
	   		<a href="<c:url value="/login.action"/>"><img src="${pageContext.servletContext.contextPath}/pagesImage/SignIn.jpg" alt="Sign In"></img></a>
	   		<a href="<c:url value="/signUp.action"/>"><img src="${pageContext.servletContext.contextPath}/pagesImage/SignUp.jpg" alt="Sign Up"></img></a>
   		</div>
	</sec:authorize>
	
	<img src="${pageContext.servletContext.contextPath}/pagesImage/welcome.jpg"></img>
	<hr>
	
	<p><address><a class="vk" href="http://vk.com/saintdaemon">Created by Serhiy Pachkovskiy &#169</a></address></p>
	
	<!-- LeftBar -->
	<div class="leftBar">
	
		<div class="left1">
			<a href="<c:url value="/index.action"/>">Home</a>
		</div>
		
		<div class="left2">
			<a href="<c:url value="/hotels/hotels.action"/>">Hotels</a>
		</div>
		
		<sec:authorize access="isAuthenticated()">
			<div class="left3">
				<a href="<c:url value="/reservations/reservations.html"/>">Reservation list</a>
			</div>
		</sec:authorize>
		
		<sec:authorize access="hasRole('ADMIN')">
			<div class="left4">
				<a href="<c:url value="/admin/hotelsInManagement.html?id=${user.id}"/>">Management</a>
			</div>
		</sec:authorize>
		
	</div>