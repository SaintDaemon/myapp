<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ page session="false"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Hotel Booking</title>
	<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/styles/styles.css"/>
	<%@include file="./styles.jsp" %>
</head>

<body>

	<%@include file="./headAndLeftPanel.jsp" %>
		
	<div  class="rightBlock">
		<div class="error">
			<pre><h3>You must <a href="/HotelBooking/login.action">log in</a> as admin  for this function!</h3></pre>
			<pre><h3>If you don't have your own account, you can <a href="/HotelBooking/signUp.action">sign up</a></h3></pre>
		</div>
	</div>
</body>
</html>