<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ page session="false"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Hotel Booking</title>
	<script src="${pageContext.servletContext.contextPath}/scripts/jquery-latest.js"></script>
	<script type="text/javascript" src="${pageContext.servletContext.contextPath}/scripts/jquery.validate.js"></script>
	<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/styles/styles.css"/>
	<script>
			$(document).ready(function(){
				$("#form").validate({
					messages: {
						name: "	Please enter name",
						description: "	Please enter description",
						country: "	Please enter your country",
						city: "	Please enter your sity"
					}
				});
			});
	</script>
	<%@include file="../styles.jsp" %>
</head>

<body>

	<%@include file="../headAndLeftPanel.jsp" %>
		
	<div  class="rightBlock">
		<h3>Enter information about your hotel</h3>
		<form id="form" action="/HotelBooking/admin/saveHotel.html">
			Name
			<p><input type="text" name="name" class="required"></input></p>
			<input type="hidden" name="id"></input>
			Description
			<p><textarea rows="4" cols="50" name="description" class="required"></textarea></p>
			Country
			<p><input type="text" name="country" class="required"></input></p>
			City
			<p><input type="text" name="city" class="required"></input></p>
			Priority
		    <select name="priority" class="star">
		        <option value="ONE">&#9733</option>
		        <option value="TWO">&#9733 &#9733</option>
		        <option value="THREE">&#9733 &#9733 &#9733 </option>
		        <option value="FOUR">&#9733 &#9733 &#9733 &#9733</option>
		        <option value="FIVE">&#9733 &#9733 &#9733 &#9733 &#9733</option>
		    </select><br>
			<input type="hidden" name="idUser" value="${user.id}"></input>
			
			<p><input type="submit" value="Ok"></input></p>
		</form>
	</div>
	
</body>
</html>