<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ page session="false"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Hotel Booking</title>
	<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/styles/styles.css"/>
	<%@include file="../styles.jsp" %>
</head>

<body>

	<%@include file="../headAndLeftPanel.jsp" %>
		
	<div  class="rightBlock">
		<h3>Hotels reservations</h3>
		<c:forEach var="reserv" items="${reservList}">
			<p><pre>Date of entry:  ${reserv.dateIn}  Check-out: ${reserv.dateOut}  Name: ${reserv.name}  Email: ${reserv.email}  Phone: ${reserv.telephone}  <a href="/HotelBooking/admin/reservedRoomInManagement.html?id=${reserv.id}">Show room</a></pre></p>
		</c:forEach>
	</div>
</body>
</html>