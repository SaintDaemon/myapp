<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ page session="false"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Hotel Booking</title>
	<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/styles/styles.css"/>
	<script src="${pageContext.servletContext.contextPath}/scripts/jquery-latest.js"></script>
	<script type="text/javascript" src="${pageContext.servletContext.contextPath}/scripts/jquery.validate.js"></script>
	<script src="${pageContext.servletContext.contextPath}/scripts/room.js" type="text/javascript"></script>
	<%@include file="../styles.jsp" %>
</head>

<body onload="doSelType()">

	<%@include file="../headAndLeftPanel.jsp" %>
		
	<div  class="rightBlock">
		<h3>You can edit the data</h3>
		<form id="form" action="/HotelBooking/admin/saveEditedRoom.html">
			Number
			<p><input type="text" name="number" class="required" value="${room.number}" onchange="go()"></input>
			<span id="msg"></span></p>
			Price
			<p><input type="text" name="price" class="required" value="${room.price}" onchange="go2()"></input>
			<span id="msg2"></span></p>
			Access
			<p>TRUE<input type="radio" name="access" value="true" checked></input></p>
			<p>FALSE<input type="radio" name="access"  class="required" value="false"></input></p>
			<input type="hidden" name="idHotel" value="${idHotel}"></input>
			<input type="hidden" name="id" value="${room.id}"></input>
			<input type="hidden" name="roomType" value="${roomType}"></input>
		    <p>Room type <select id="sel" name="type">
		        <option value="Single">Single(1)</option>
		        <option value="Double">Double(2)</option>
		        <option value="Twin">Twin(2)</option>
		        <option value="Triple">Triple(3)</option>
		        <option value="Quadruple">Quadruple(4)</option>
		    </select></p>
		    
			<p><input id="submit" type="submit" value="Ok"></input></p>
			<img class="l" id="loading" src="${pageContext.servletContext.contextPath}/pagesImage/loading.gif" alt="loading">
		</form>
	</div>
</body>
</html>