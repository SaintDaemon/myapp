<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ page session="false"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Hotel Booking</title>
	<script src="${pageContext.servletContext.contextPath}/scripts/jquery-latest.js"></script>
	<script type="text/javascript" src="${pageContext.servletContext.contextPath}/scripts/jquery.validate.js"></script>
	<script src="${pageContext.servletContext.contextPath}/scripts/room.js" type="text/javascript"></script>
	<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/styles/styles.css"/>
	
		<script>
			$(document).ready(function(){
				$("#form").validate({
					messages: {
						number: "	Please enter room number",
						price: "	Please enter room price"
					}
				});
			});
		</script>
	<%@include file="../styles.jsp" %>
</head>

<body>

	<%@include file="../headAndLeftPanel.jsp" %>
		
	<div  class="rightBlock">
		<h3>Enter information about your room</h3>
		<form id="form" action="/HotelBooking/admin/saveRoom.html">
			Number
			<p><input type="text" name="number" class="required" onchange="go()"></input>
			<span id="msg"></span></p>
			Price
			<p><input type="text" name="price" class="required" onchange="go2()"></input>
			<span id="msg2"></span></p>
			Access
			<p>TRUE<input type="radio" name="access" value="true" checked></input></p>
			<p>FALSE<input type="radio" name="access"  class="required" value="false"></input></p>
			<input type="hidden" name="idHotel" value="${hotel.id}"></input>
			<input type="hidden" name="id"></input>
		    <p>Room type <select name="type">
		        <option value="Single">Single(1)</option>
		        <option value="Double">Double(2)</option>
		        <option value="Twin">Twin(2)</option>
		        <option value="Triple">Triple(3)</option>
		        <option value="Quadruple">Quadruple(4)</option>
		    </select></p>
			<p><input id="submit" type="submit" value="Ok"></input></p>
			
			<div class="img">
				<img class="l" id="loading" src="${pageContext.servletContext.contextPath}/loading.gif" alt="loading">
			</div>
		</form>
	</div>
	
</body>
</html>