<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ page session="false"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Hotel Booking</title>
	<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/styles/styles.css"/>
	<script src="${pageContext.servletContext.contextPath}/scripts/jquery-latest.js"></script>
	<script type="text/javascript" src="${pageContext.servletContext.contextPath}/scripts/jquery.validate.js"></script>
	<script src="${pageContext.servletContext.contextPath}/scripts/hotel.js" type="text/javascript"></script>
		
	<script>
		$(document).ready(function(){
			$("#form").validate({
				messages: {
					name: "	Please enter hotel name",
					description: "	Please enter hotel description",
					country: "	Please enter hotel country",
					city: "	Please enter hotel city"
				}
			});
		});
	</script>
	
	<%@include file="../styles.jsp" %>
</head>

<body onload="doSelPriority()">

	<%@include file="../headAndLeftPanel.jsp" %>
		
	<div  class="rightBlock">
		<h3>You can edit the data</h3>
		<form id="form" action="/HotelBooking/admin/saveEditedHotel.html">
			Name
			<p><input type="text" value="${hotel.name}" name="name" class="required" onchange="go()"></input></p>
			<span id="msg"></span></p>
			Description
			<p><input type="text" value="${hotel.description}" name="description" class="required"></input>
			<span id="msg"></span></p>
			Country
			<p><input type="text" value="${hotel.country}" name="country" class="required"></input></p>
			City
			<p><input type="text" value="${hotel.city}" name="city" class="required"></input></p>
			<input type="hidden" value="${hotel.id}" name="id"></input>
			Priority
		    <select id="sel" name="priority" class="star">
		        <option value="ONE">&#9733</option>
		        <option value="TWO">&#9733 &#9733</option>
		        <option value="THREE">&#9733 &#9733 &#9733 </option>
		        <option value="FOUR">&#9733 &#9733 &#9733 &#9733</option>
		        <option value="FIVE">&#9733 &#9733 &#9733 &#9733 &#9733</option>
		    </select><br>
		    <input type="hidden" name="idUser" value="${user.id}"></input>
		    <input type="hidden" name="hotelPriority" value="${hotelPriority}"></input>
		    
			<p><input id="submit" type="submit" value="Save"></input></p>
			<img class="l" id="loading" src="${pageContext.servletContext.contextPath}/loading.gif" alt="loading">
		</form>
	</div>

</body>
</html>