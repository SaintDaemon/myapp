<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ page session="false"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Hotel Booking</title>
	<script src="${pageContext.servletContext.contextPath}/scripts/jquery-latest.js"></script>
	<script type="text/javascript" src="${pageContext.servletContext.contextPath}/scripts/jquery.validate.js"></script>
	<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/styles/styles.css"/>
	<script>
		$(document).ready(function(){
			$("#form").validate({
				messages: {
					file: ""
				}
			});
		});
	</script>
	<%@include file="../styles.jsp" %>
</head>

<body>

	<%@include file="../headAndLeftPanel.jsp" %>
		
	<div  class="rightBlock">
		<form id="form" enctype="multipart/form-data" method="post" action="/HotelBooking/admin/saveImage.html?address=${pageContext.servletContext.contextPath}&idHotel=${hotel.id}&idRoom=${room.id}">
			<p>Load image</p>
				<p><input type="file" name="file" accept="image/*,image/jpeg" class="required">
				<input type="submit" value="load"></p>
		</form>
	</div>
</body>
</html>