<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ page session="false"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Hotel Booking</title>
	<script src="${pageContext.servletContext.contextPath}/scripts/jquery-latest.js"></script>
	<script type="text/javascript" src="${pageContext.servletContext.contextPath}/scripts/jquery.validate.js"></script>
	<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/styles/styles.css"/>
	<script>
		$(document).ready(function(){
			$("#form").validate({
				messages: {
					password: "	Please enter room password",
				}
			});
		});
	</script>
	
	<%@include file="../styles.jsp" %>
</head>

<body>

	<%@include file="../headAndLeftPanel.jsp" %>
		
	<div  class="rightBlock">
	
		<form id="form" action="/HotelBooking/admin/deleteHotel.html">
			<h3>If you really want to do this, enter the password and press OK</h3>
			Password: <input type="text" name="password" class="required"></input>
			<p><input id="submit" type="submit" value="Ok"></input></p>
			<p><input type="hidden" name="idHotel" value="${hotel.id}"></input></p>
			<p><input type="hidden" name="idUser" value="${user.id}"></input></p>
		</form>
		
		<div class="img">
			<img src="${pageContext.servletContext.contextPath}/pagesImage/wrong.jpg" alt="success" width="200"></img>
		</div>
		
	</div>
</body>
</html>