<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ page session="false"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Hotel Booking</title>
	<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/styles/styles.css"/>
	<%@include file="../styles.jsp" %>
</head>

<body>

	<%@include file="../headAndLeftPanel.jsp" %>
		
	<div  class="rightBlock">
		<h3>List of your hotels</h3>
		
		<h3><a href="<c:url value="/admin/addHotel.html?id=${user.id}"/>">Add hotel</a></h3>
		
		<c:forEach var="hotel" items="${personalHotelList}">
			<pre><a href="<c:url value="/admin/hotelInManagement.html?id=${hotel.id}"/>" class="list">${hotel.name} (${hotel.city} ${hotel.country}) ${hotel.priorityView}</a><a href="<c:url value="/admin/deleteHotelCheck.html?idHotel=${hotel.id}"/>">Delete hotel</a></pre>
		</c:forEach>
		
	</div>
</body>
</html>