<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ page session="false"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Hotel Booking</title>
	
	<script src="${pageContext.servletContext.contextPath}/scripts/jquery-latest.js"></script>
	<script src="${pageContext.servletContext.contextPath}/scripts/jquery-ui.js"></script>
	<script src="${pageContext.servletContext.contextPath}/scripts/search.js" type="text/javascript"></script>
	<link rel="stylesheet" href="${pageContext.servletContext.contextPath}/styles/jquery-ui.css" />
	<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/styles/styles.css"/>
	
	<script>
		$(function() {
	    	$( "#datepicker" ).datepicker({
	    		minDate: new Date(),
	    		maxDate: "+6m",
	    		closeText: "Close"
	    	});
	  	});
		$(function() {
	    	$( "#datepicker2" ).datepicker({
	    		minDate: new Date(),
	    		maxDate: "+6m"
	    	});
	  	});
	</script>
	
	<%@include file="./styles.jsp" %>
	
</head>

<body>
	
	<%@include file="./headAndLeftPanel.jsp" %>
	
	<!-- SERSRCH -->
	<form action="search/search.action?sort=asc">
		<div class="search">
		
			<h2 align="right">Search</h2>
			
			<h4 class="white" align="right">There are ${hotels} hotels at your disposal</h4>
			
			<h4>Name/Place:</h4>
			
			<p><input type="text" name="name/place" size="20" placeholder="city, country, hotel name"></input></p>
			
			<h3><pre>Date of entry:	  Check-out date:</pre></h3>
			
			<p><pre><input type="text" id="datepicker" name="datepicker"  size="9" readonly onchange="date()"/>	     <input type="text" id="datepicker2" name="datepicker2" size="9" readonly onchange="date2()"/></input></pre></p>
		    
		    <h3><pre>Room type:   <select name="type">
		        <option value="type">type</option>
		        <option value="Single">Single(1)</option>
		        <option value="Double">Double(2)</option>
		        <option value="Twin">Twin(2)</option>
		        <option value="Triple">Triple(3)</option>
		        <option value="Quadruple">Quadruple(4)</option>
		    
		    </select>		<span id="msg3" class="red"></span></pre></h3>
		    
		    <p align="right"><input id="submit" type="submit" value="OK"></input></p>
		</div>
	</form>
	
	<p><address><a class="vk" href="http://vk.com/saintdaemon">Created by Serhiy Pachkovskiy &#169</a></address></p>
</body>
</html>