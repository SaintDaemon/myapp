<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ page session="false"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Sign In</title>
	<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/styles/styles.css"/>
	<%@include file="./styles.jsp" %>
</head>

<body>

	<%@include file="./headAndLeftPanel.jsp" %>

	<div class="loginBlock" align="center">
		<h3>Please enter your username and password to login</h3>
		<p><pre>If you don't have your own account, you can <a href="signUp.action">sign up</a></pre>
		
		<form id="form" action="<c:url value='j_spring_security_check'/>" method='POST'>
			Username
			<p><input type="text" name="j_username"></input></p>
			Password
			<p><input type="text" name="j_password"></input></p>
			<p><input type="submit" value="Ok"></input></p>
		</form>
	</div>
	<div class="img">
		<img src="${pageContext.servletContext.contextPath}/pagesImage/wrong.jpg" alt="success" width="200"></img>
	</div>
</body>
</html>