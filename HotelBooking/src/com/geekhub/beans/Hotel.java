package com.geekhub.beans;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.IndexColumn;

@Entity
@Table(name="Hotel")
public class Hotel implements Comparable<Hotel>{
	
	@GeneratedValue
	@Id
	@Column(name="ID")
	private Integer id;
	
	@Column(name="name")
	private String name;
	
	@Column(name="description")
	private String description;

	@ManyToOne
	@JoinColumn(name="idOwner", insertable=false, updatable=false, nullable=false)
	private User owner;
	
	@Column(name="country")
	private String country;
	
	@Column(name="city")
	private String city;
	
	@Column(name="priority")
	private HotelPriority priority;
	
	@Column(name="priorityView")
	private String priorityView;
	
	@OneToMany(cascade={CascadeType.ALL})
	@JoinColumn(name="idHotel")
	@IndexColumn(name="idx")
	private List<Room> rooms;
	
	@OneToMany(cascade={CascadeType.ALL})
	@JoinColumn(name="idHotel")
	@IndexColumn(name="idxH")
	private List<Image> images;
	
	public String getPriorityView() {
		return priorityView;
	}
	public void setPriorityView(HotelPriority priority) {
		priorityView = "&#9733 ";
		for(int i = 1; i < priority.priority; i++){
			priorityView = priorityView + "&#9733 ";
		}
	}
	
	public List<Image> getImages() {
		return images;
	}
	public void setImages(List<Image> images) {
		this.images = images;
	}
	public User getOwner() {
		return owner;
	}
	public void setOwner(User owner) {
		this.owner = owner;
	}
	public List<Room> getRooms() {
		return rooms;
	}
	public void setRooms(List<Room> rooms) {
		this.rooms = rooms;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public HotelPriority getPriority() {
		return priority;
	}
	public void setPriority(HotelPriority priority) {
		this.priority = priority;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Hotel other = (Hotel) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	@Override
	public int compareTo(Hotel hotel) {
		if(this.getPriority().priority < hotel.getPriority().priority){
			return -1;
		} else {
			if(this.getPriority().priority == hotel.getPriority().priority){
				return 0;
			} else {
				return 1;
			}
		}
	}

	
}
