package com.geekhub.beans;

public enum HotelPriority {
	 ONE(1),
	 TWO(2),
	 THREE(3),
	 FOUR(4),
	 FIVE(5);

	 public Integer priority;

	 private HotelPriority(Integer priority) {
		 this.priority = priority;
	 }
}
