package com.geekhub.beans;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

public class SearchResult {

	private ArrayList<Hotel> hotelList;
	private Calendar dateIn;
	private Calendar dateOut;
	
	private HashMap<Integer, ArrayList<Room>> roomMap;
	public Calendar getDateIn() {
		return dateIn;
	}
	public void setDateIn(Calendar dateIn) {
		this.dateIn = dateIn;
	}
	public Calendar getDateOut() {
		return dateOut;
	}
	public void setDateOut(Calendar dateOut) {
		this.dateOut = dateOut;
	}
	public ArrayList<Hotel> getHotelList() {
		return hotelList;
	}
	public void setHotelList(ArrayList<Hotel> hotelList) {
		this.hotelList = hotelList;
	}
	public HashMap<Integer, ArrayList<Room>> getRoomMap() {
		return roomMap;
	}
	public void setRoomMap(HashMap<Integer, ArrayList<Room>> roomMap) {
		this.roomMap = roomMap;
	}
	
}
