package com.geekhub.beans;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.IndexColumn;

@Entity
@Table(name="Room")
public class Room {
	
	@GeneratedValue
	@Id
	@Column(name="ID")
	private Integer id;
	
	@Column(name="number")
	private Integer number;
	
	@Column(name="type")
	private RoomType type;
	
	@Column(name="price")
	private Integer price;
	
	@ManyToOne
	@JoinColumn(name="idHotel", insertable=false, updatable=false, nullable=false)
	private Hotel Hotel;

	
	@Column(name="access")
	private boolean access;
	
	public List<Image> getImages() {
		return images;
	}

	public void setImages(List<Image> images) {
		this.images = images;
	}

	@OneToMany(cascade={CascadeType.ALL})
	@JoinColumn(name="idRoom")
	@IndexColumn(name="idx")
	private List<Reservation> reservations;
	
	@OneToMany(cascade={CascadeType.ALL})
	@JoinColumn(name="idRoom")
	@IndexColumn(name="idxR")
	private List<Image> images;
	
	public List<Reservation> getReservations() {
		return reservations;
	}

	public void setReservations(List<Reservation> reservations) {
		this.reservations = reservations;
	}

	public Hotel getHotel() {
		return Hotel;
	}

	public void setHotel(Hotel hotel) {
		Hotel = hotel;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}

	public Integer getPrice() {
		return price;
	}

	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	public boolean isAccess() {
		return access;
	}

	public void setAccess(boolean access) {
		this.access = access;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public RoomType getType() {
		return type;
	}

	public void setType(RoomType type) {
		this.type = type;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Room other = (Room) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}
