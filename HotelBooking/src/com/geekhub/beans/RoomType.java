package com.geekhub.beans;

public enum RoomType {
	Single(1), Double(2), Twin(3), Triple(4), Quadruple(5);
	
	 public Integer type;

	 private RoomType(Integer type) {
		 this.type = type;
	 }
}
