package com.geekhub.beans;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="Reservation")
public class Reservation {
	
	@GeneratedValue
	@Id
	@Column(name="id")
	private Integer id;

	@ManyToOne
	@JoinColumn(name="idUser", insertable=false, updatable=false, nullable=false)
	private User User;
	
	@Column(name="date_in")
	private Date dateIn;
	
	@Column(name="date_out")
	private Date dateOut;

	@ManyToOne
	@JoinColumn(name="idRoom", insertable=false, updatable=false, nullable=false)
	private Room room;
	
	@Column(name="name")
	private String name;
	
	@Column(name="email")
	private String email;
	
	@Column(name="telephone")
	private String telephone;

	@Column(name="topicality")
	private boolean topicality = true;
	
	public Room getRoom() {
		return room;
	}

	public void setRoom(Room room) {
		this.room = room;
	}

	public boolean isTopicality() {
		return topicality;
	}

	public void setTopicality(boolean topicality) {
		this.topicality = topicality;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public User getUser() {
		return User;
	}

	public void setUser(User user) {
		User = user;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getDateOut() {
		return dateOut;
	}

	public void setDateOut(java.util.Date date2) {
		this.dateOut = (Date) date2;
	}

	public Date getDateIn() {
		return dateIn;
	}

	public void setDateIn(java.util.Date date) {
		this.dateIn = (Date) date;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Reservation other = (Reservation) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
