package com.geekhub.services;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.geekhub.beans.Reservation;
import com.geekhub.beans.Room;
import com.geekhub.beans.User;

@Repository
@Transactional
public class ReservationDAOImpl implements ReservationDAO{

	@Autowired SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	@Override
	public List<Reservation> getReservationListByIdRoom(Integer id) {
		Session session = sessionFactory.getCurrentSession();
		String hgl = "select reservation from Reservation reservation where idRoom = :idRoom and topicality = :topicality";
		return session.createQuery(hgl).setInteger("idRoom", id).setBoolean("topicality", true).list();
	}

	@Override
	public void saveReservation(Reservation reservation, Integer idRoom, Integer idUser) {
		Session session = sessionFactory.openSession();
		if(idUser != null){
			session.beginTransaction();
			User user = (User) session.get(User.class, idUser);
			List<Reservation> reservationsU = user.getReservations();
			if(reservationsU.contains(reservation)){
				reservationsU.remove(reservation);
			}
			session.getTransaction().commit();
			session.close();
			
			reservationsU.add(reservation);
			user.setReservations(reservationsU);
			
			session = sessionFactory.openSession();
			session.beginTransaction();
			session.saveOrUpdate(user);
			session.getTransaction().commit();
			session.close();
		}
		session = sessionFactory.openSession();
		session.beginTransaction();
		Room room = (Room) session.get(Room.class, idRoom);
		List<Reservation> reservations = room.getReservations();
		if(reservations.contains(reservation)){
			reservations.remove(reservation);
		}
		session.getTransaction().commit();
		session.close();
		
		reservations.add(reservation);
		room.setReservations(reservations);
		
		session = sessionFactory.openSession();
		session.beginTransaction();
		session.saveOrUpdate(room);
		session.getTransaction().commit();
		session.close();
	}

	@Override
	public void deleteReservation(Integer id){
		Reservation reservation = getReservationById(id);
		if(null != reservation)
			sessionFactory.getCurrentSession().delete(reservation);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Reservation> getReservationListByIdUser(Integer id) {
		return (List<Reservation>) sessionFactory.getCurrentSession().createQuery("from Reservation where topicality = " + true + " and idUser = " + id).list();
	}

	@Override
	public Reservation getReservationById(Integer id) {
		return (Reservation) sessionFactory.getCurrentSession().createQuery("from Reservation where id = " + id).uniqueResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Reservation> getReservationList() {
		return (List<Reservation>) sessionFactory.getCurrentSession().createQuery("from Reservation where topicality = " + true).list();
	}
	
	@SuppressWarnings("deprecation")
	public void checkingReservation() {
		System.out.println("Start checking Reservationend");
		ArrayList<Reservation> rList = (ArrayList<Reservation>) getReservationList();
		if(rList != null){
			Calendar c = Calendar.getInstance();	
			Iterator<Reservation> iter = rList.iterator();
			while(iter.hasNext()){
				Reservation r = iter.next();
				if(c.get(Calendar.YEAR) > r.getDateOut().getYear() || 
						(c.get(Calendar.YEAR) == r.getDateOut().getYear() && c.get(Calendar.MONTH) > r.getDateOut().getMonth()) ||
						(c.get(Calendar.YEAR) == r.getDateOut().getYear() && c.get(Calendar.MONTH) == r.getDateOut().getMonth() && c.get(Calendar.DATE) == r.getDateOut().getDate())){
					deleteReservation(r.getId());
				}
			}
		}
		System.out.println("End of the test Reservation!");
	}

}
