package com.geekhub.services;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.geekhub.beans.Hotel;
import com.geekhub.beans.Image;
import com.geekhub.beans.Room;
import com.geekhub.beans.User;

@Repository
@Transactional
public class HotelDaoImpl implements HotelDAO{
	
	@Autowired SessionFactory sessionFactory;
	@Autowired UserDAO userDAO;
	@Autowired RoomDAO roomDAO;
	@Autowired ImageDAO imageDAO;
	final String address = "C:\\Users\\SaintDaemon\\git\\myApp\\HotelBooking\\WebContent";

	@SuppressWarnings("unchecked")
	
	@Override
	public List<Hotel> getHotelList() {
		return sessionFactory.getCurrentSession().createCriteria(Hotel.class).list();
	}

	@Override
	public Hotel getHotelById(Integer id) {
		Session session = sessionFactory.getCurrentSession();
		return (Hotel) session.createQuery("from Hotel where id = " + id).uniqueResult();
	}

	@Override
	//TODO
	public void deleteHotel(Integer id) {
		Hotel hotel = getHotelById(id);
		if(null != hotel){
			List<Image> images = imageDAO.getImageListByIdHotel(id);
			for(Image i: images){
				File img = new File(address + "\\images\\" + imageDAO.getImageById(i.getId()).getContent());
				if(img.exists()){
					img.delete();
				}
				sessionFactory.getCurrentSession().delete(i);
			}
			List<Room> rooms = roomDAO.getRoomListByIdHotel(id);
			for(Room r: rooms){
				images = imageDAO.getImageListByIdRoom(r.getId());
				for(Image i: images){
					File img = new File(address + "\\images\\" + imageDAO.getImageById(i.getId()).getContent());
					if(img.exists()){
						img.delete();
					}
					sessionFactory.getCurrentSession().delete(i);
				}
			}
			sessionFactory.getCurrentSession().delete(hotel);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void saveHotel(Hotel hotel, Integer idUser) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		User user = (User) session.get(User.class, idUser);
		List<Hotel> hotels = user.getHotels();
		int i = 0;
		if(hotels.contains(hotel)){
			i = 1;
		}
		session.getTransaction().commit();
		session.close();
		
		
		session = sessionFactory.openSession();
		session.beginTransaction();
		if(i == 0){
			hotels.add(hotel);
		} else {
			hotel.setRooms(sessionFactory.getCurrentSession().createQuery("from Room where idHotel = " + hotel.getId()).list());
			session.getTransaction().commit();
			session.close();
			
			session = sessionFactory.openSession();
			session.beginTransaction();
			hotel.setImages(sessionFactory.getCurrentSession().createQuery("from Image where idHotel = " + hotel.getId()).list());
			session.getTransaction().commit();
			session.close();
		}
		
		session = sessionFactory.openSession();
		session.beginTransaction();
		hotels.remove(hotel);
		hotels.add(hotel);
		user.setHotels(hotels);
		
		session = sessionFactory.openSession();
		session.beginTransaction();
		session.saveOrUpdate(user);
		session.getTransaction().commit();
		session.close();

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Hotel> getPersonalHotelList(Integer id) {
		return (ArrayList<Hotel>) sessionFactory.getCurrentSession().createQuery("from Hotel where idOwner=" + id).list();
	}
	
	@Override
	public Hotel getHotelByName(String name) {
		Session session = sessionFactory.getCurrentSession();
		return (Hotel) session.createCriteria(Hotel.class).add(Restrictions.like("name", name)).uniqueResult();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Hotel> getHotelListByNamePlace(String namePlace) {
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Hotel.class).add(Restrictions.disjunction()
		        .add(Restrictions.eq("name", namePlace))
		        .add(Restrictions.eq("country", namePlace))
		        .add(Restrictions.eq("city", namePlace)));
		return criteria.list();
	}

}
