package com.geekhub.services;

import com.geekhub.beans.User;

public interface UserDAO {

	public User getUserById(Integer id);
	
	public void saveUser(User user);
	
	public User getUserByUsername(String username);
	
	public User getCurrentUser();
	
	public String encryptString(String password);
}
