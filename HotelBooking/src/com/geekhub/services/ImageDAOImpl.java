package com.geekhub.services;

import java.io.File;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.geekhub.beans.Hotel;
import com.geekhub.beans.Image;
import com.geekhub.beans.Room;

@Repository
@Transactional
public class ImageDAOImpl implements ImageDAO{
	
	@Autowired SessionFactory sessionFactory;
	final String address = "C:\\Users\\SaintDaemon\\git\\myApp\\HotelBooking\\WebContent";

	@SuppressWarnings("unchecked")
	@Override
	public List<Image> getImageListByIdHotel(Integer id) {
		return sessionFactory.getCurrentSession().createQuery("from Image where idHotel = " + id).list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Image> getImageListByIdRoom(Integer id) {
		return sessionFactory.getCurrentSession().createQuery("from Image where idRoom = " + id).list();
	}

	@Override
	public void deleteImage(Integer id) {
		Image image = getImageById(id);
		if(null != image){
			File img = new File(address + "\\images\\" + getImageById(id).getContent());
			if(img.exists()){
				img.delete();
			}
			sessionFactory.getCurrentSession().delete(image);
		}
	}

	@Override
	public Image getImageById(Integer id) {
		return (Image) sessionFactory.getCurrentSession().createQuery("from Image where id = " + id).uniqueResult();
	}
	
	@Override
	public void saveImage(Image image, Integer idRoom, Integer idHotel) {
		if(idRoom != null){
			saveRoomImage(image, idRoom);
		} else {
			saveHotelImage(image, idHotel);
		}
	}

	public void saveRoomImage(Image image, Integer idRoom) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		Room room = (Room) session.get(Room.class, idRoom);
		List<Image> images = room.getImages();
		images.add(image);
		room.setImages(images);
		session.saveOrUpdate(room);
		session.getTransaction().commit();
		session.close();
	}
	
	
	public void saveHotelImage(Image image, Integer idHotel) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		Hotel hotel = (Hotel) session.get(Hotel.class, idHotel);
		List<Image> images = hotel.getImages();
		images.add(image);
		hotel.setImages(images);
		session.saveOrUpdate(hotel);
		session.getTransaction().commit();
		session.close();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Image> getImageListBy() {
		return (List<Image>) sessionFactory.getCurrentSession().createQuery("from Image").list();
	}
	
}
