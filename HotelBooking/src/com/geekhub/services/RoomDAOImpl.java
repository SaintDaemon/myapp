package com.geekhub.services;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.geekhub.beans.Hotel;
import com.geekhub.beans.Image;
import com.geekhub.beans.Room;

@Repository
@Transactional
public class RoomDAOImpl implements RoomDAO {
	
	@Autowired SessionFactory sessionFactory;
	@Autowired ReservationDAO reservationDAO;
	@Autowired ImageDAO imageDAO;
	@Autowired HotelDAO hotelDAO;
	final String address = "C:\\Users\\SaintDaemon\\git\\myApp\\HotelBooking\\WebContent";

	@Override
	public Room getRoomById(Integer id) {
		Session session = sessionFactory.getCurrentSession();
		return (Room) session.createQuery("from Room where id = " + id).uniqueResult();
	}

	@Override
	public void deleteRoom(Integer id) {
		Room room = getRoomById(id);
		if(null != room){
			List<Image> images = imageDAO.getImageListByIdRoom(id);
			for(Image i: images){
				File img = new File(address + "\\images\\" + imageDAO.getImageById(i.getId()).getContent());
				if(img.exists()){
					img.delete();
				}
				sessionFactory.getCurrentSession().delete(i);
			}
			sessionFactory.getCurrentSession().delete(room);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void saveRoom(Room room, Integer idHotel) {
		
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		System.out.println(idHotel);
		Hotel hotel = (Hotel) session.createQuery("from Hotel where id = " + idHotel).uniqueResult();
		System.out.println(hotel.getId());
		List<Room> rooms = hotel.getRooms();
		int i = 0;
		if(rooms.contains(room)){
			i = 1;
		}
		session.getTransaction().commit();
		session.close();
		
		if(i == 0){
			rooms.add(room);
		} else {
			room.setReservations(sessionFactory.getCurrentSession().createQuery("from Reservation where idRoom = " + room.getId()).list());
			room.setImages(sessionFactory.getCurrentSession().createQuery("from Image where idRoom = " + room.getId()).list());
		}
		
		rooms.remove(room);
		rooms.add(room);
		hotel.setRooms(rooms);
		
		session = sessionFactory.openSession();
		session.beginTransaction();
		session.saveOrUpdate(hotel);
		session.getTransaction().commit();
		session.close();
		
	}

	@Override
	public Room getRoomByNumber(Integer number) {
		Session session = sessionFactory.getCurrentSession();
		return (Room) session.createCriteria(Room.class).add(Restrictions.like("number", number)).uniqueResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Room> getRoomListByIdHotel(Integer id) {
		return (ArrayList<Room>) sessionFactory.getCurrentSession().createQuery("from Room where idHotel=" + id).list();
	}

}
