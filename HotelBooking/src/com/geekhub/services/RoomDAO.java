package com.geekhub.services;

import java.util.List;

import com.geekhub.beans.Room;

	public interface RoomDAO {
		
	public Room getRoomById(Integer id);
	
	public void deleteRoom(Integer id);
	
	public Room getRoomByNumber(Integer number);

	public List<Room> getRoomListByIdHotel(Integer id);
	
	void saveRoom(Room room, Integer idHotel);
	
}
