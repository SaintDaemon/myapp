package com.geekhub.services;

import java.util.List;

import com.geekhub.beans.Reservation;

public interface ReservationDAO {
	
	public List<Reservation> getReservationListByIdRoom(Integer id);
	
	public List<Reservation> getReservationListByIdUser(Integer id);
	
	public List<Reservation> getReservationList();

	public Reservation getReservationById(Integer id);
	
	public void deleteReservation(Integer id);
	
	public void checkingReservation();

	public void saveReservation(Reservation reservation, Integer idRoom, Integer idUser);

}
