package com.geekhub.services;

import java.util.List;

import com.geekhub.beans.Image;

public interface ImageDAO {

	public List<Image> getImageListByIdHotel(Integer id);
	
	public List<Image> getImageListByIdRoom(Integer id);
	
	public Image getImageById(Integer id);
	
	public void deleteImage(Integer id);
	
	public void saveImage(Image image, Integer idRoom, Integer idHotel);

	public List<Image> getImageListBy();
	
}
