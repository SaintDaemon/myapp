package com.geekhub.services;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.logi.crypto.sign.Fingerprint;
import org.logi.crypto.sign.MD5State;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.geekhub.beans.User;

@Repository
@Transactional
public class UserDAOImpl implements UserDAO {
	
	@Autowired SessionFactory sessionFactory;
	
	@Override
	public User getUserById(Integer id) {
		Session session = sessionFactory.getCurrentSession();
		return (User) session.createQuery("from User where id = " + id).uniqueResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public void saveUser(User user) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		user.setReservations(session.createQuery("from Reservation where idUser = " + user.getId()).list());
		session.getTransaction().commit();
		session.close();
		
		session = sessionFactory.openSession();
		session.beginTransaction();
		user.setHotels(session.createQuery("from Hotel where idOwner = " + user.getId()).list());
		session.getTransaction().commit();
		session.close();
		
		sessionFactory.getCurrentSession().saveOrUpdate(user);
	}
	
	@Override
	public User getUserByUsername(String username) {
		Session session = sessionFactory.getCurrentSession();
		return (User) session.createCriteria(User.class).add(Restrictions.like("username", username)).uniqueResult();
	}

	@Override
	public User getCurrentUser() {
		String username = SecurityContextHolder.getContext().getAuthentication().getName();
		if(!"anonymousUser".equals(username)){
			return getUserByUsername(username);
		}
		return null;
	}
	
	
	@Override
	public String encryptString(String sourseString) {
		 if (sourseString == null){
			 return null;
		 } else {
			 MD5State digest = new MD5State();
			 digest.update(sourseString.getBytes());
			 Fingerprint hash = digest.calculate();
			 String encryptedString = hash.toString();
	
			 encryptedString = encryptedString.substring(encryptedString.indexOf(",")+1, encryptedString.length()-1);
			 return encryptedString;
		 }
	}


}
