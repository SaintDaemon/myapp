package com.geekhub.services;

import java.util.List;

import com.geekhub.beans.Hotel;

public interface HotelDAO {

	public List<Hotel> getHotelList();
	
	public Hotel getHotelById(Integer id);
	
	public Hotel getHotelByName(String name);
	
	public void deleteHotel(Integer id);
	
	public List<Hotel> getPersonalHotelList(Integer id);
	
	public List<Hotel> getHotelListByNamePlace(String namePlace);

	public void saveHotel(Hotel hotel, Integer idUser);

}
