package com.geekhub.controllers;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.hibernate.StaleStateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.geekhub.beans.Image;
import com.geekhub.beans.Reservation;
import com.geekhub.beans.Room;
import com.geekhub.beans.SearchResult;
import com.geekhub.beans.User;
import com.geekhub.services.HotelDAO;
import com.geekhub.services.ImageDAO;
import com.geekhub.services.ReservationDAO;
import com.geekhub.services.RoomDAO;
import com.geekhub.services.UserDAO;

@Controller
public class RoomController {
	
	@Autowired RoomDAO roomDAO;
	@Autowired HotelDAO hotelDAO;
	@Autowired UserDAO userDAO;
	@Autowired ReservationDAO reservationDAO;
	@Autowired ImageDAO imageDAO;
	final String address = "C:/Users/SaintDaemon/git/myApp/HotelBooking/WebContent";

	@RequestMapping(value="admin/roomInManagement.html")
	public String roomInManagement(@RequestParam(value="id", required=true) Integer idRoom, ModelMap map) {
		Room room = roomDAO.getRoomById(idRoom);
		map.put("room", room);
		ArrayList<Image> list = (ArrayList<Image>) imageDAO.getImageListByIdRoom(idRoom);
		map.put("images", list);
		map.put("address", address);
		map.put("user", userDAO.getCurrentUser());
		return "management/roomInManagement";
	}
	
	@RequestMapping(value="user/roomInReservations.html")
	public String roomInReservation(@RequestParam(value="id", required=true) Integer idReserv, ModelMap map) {
		Reservation res = reservationDAO.getReservationById(idReserv);
		Room room = res.getRoom();
		map.put("room", room);
		map.put("hotel", room.getHotel().getId());
		ArrayList<Image> list = (ArrayList<Image>) imageDAO.getImageListByIdRoom(room.getId());
		map.put("images", list);
		map.put("address", address);
		map.put("user", userDAO.getCurrentUser());
		return "reservations/roomInReservations";
	}
	
	@RequestMapping(value="search/roomInSearch.action")
	public String roomInSearch(@RequestParam(value="id", required=true) Integer idRoom, ModelMap map) {
		map.put("room", roomDAO.getRoomById(idRoom));
		ArrayList<Image> list = (ArrayList<Image>) imageDAO.getImageListByIdRoom(idRoom);
		map.put("images", list);
		map.put("address", address);
		map.put("user", userDAO.getCurrentUser());
		try{
			map.put("reservList", reservationDAO.getReservationListByIdUser(userDAO.getCurrentUser().getId()));
		} catch(NullPointerException e){
			System.out.println(e);
		}
		return "search/roomInSearch";
	}
	
	
	@RequestMapping(value="admin/reservedRoomInManagement.html")
	public String showRoomAdmin(@RequestParam(value="id", required=true) Integer idRes, ModelMap map){
		Reservation res = reservationDAO.getReservationById(idRes);
		map.put("room", res.getRoom());
		map.put("user", userDAO.getCurrentUser());
		return "management/reservedRoomInManagement";
	}
	
	@RequestMapping(value="search/roomsInSearch.action")
	public String showRoomsS(@RequestParam(value="id", required=true) Integer idHotel, ModelMap map, HttpServletRequest request) {
		HashMap<Integer, ArrayList<Room>> roomMap = ((SearchResult) request.getSession().getAttribute("searchResult")).getRoomMap();
		map.put("listRooms", roomMap.get(idHotel));
		map.put("user", userDAO.getCurrentUser());
		return "search/roomsInSearch";
	}
	
	@RequestMapping(value="admin/deleteRoom.html")
	public String deleteRoom(@RequestParam(value="idRoom", required=true) Integer idRoom, 
			@RequestParam(value="idHotel", required=true) Integer idHotel,
			@RequestParam(value="password", required=false) String password,
			@RequestParam(value="idUser", required=true) Integer idUser, ModelMap map) {
		if((password == null) || !(userDAO.encryptString(password).equals(userDAO.getUserById(idUser).getPassword()))){
			map.put("hotel", hotelDAO.getHotelById(idHotel));
			map.put("room", roomDAO.getRoomById(idRoom));
			map.put("user", userDAO.getCurrentUser());
			return "management/deleteRoomCheckWrong";
		} else {
			roomDAO.deleteRoom(idRoom);
			map.put("user", userDAO.getCurrentUser());
			map.put("hotel", hotelDAO.getHotelById(idHotel));
			map.put("personalRoomList", roomDAO.getRoomListByIdHotel(idHotel));
			return "management/roomsInManagement";
		}
	}
	
	@RequestMapping(value="admin/deleteRoomCheck.html")
	public String deleteRoomCheck(@RequestParam(value="idRoom", required=true) Integer idRoom, 
			@RequestParam(value="idHotel", required=true) Integer idHotel, ModelMap map) {
		map.put("user", userDAO.getCurrentUser());
		map.put("hotel", hotelDAO.getHotelById(idHotel));
		map.put("room", roomDAO.getRoomById(idRoom));
		return "management/deleteRoomCheck";
	}
	
	@RequestMapping(value="admin/saveRoom.html")
	public String saveRoom(@RequestParam(value="idHotel", required=true) Integer idHotel, Room room, ModelMap map) {
		roomDAO.saveRoom(room, idHotel);
		map.put("user", userDAO.getCurrentUser());
		map.put("hotel", hotelDAO.getHotelById(idHotel));
		map.put("personalRoomList", roomDAO.getRoomListByIdHotel(idHotel));
		return "management/roomsInManagement";
	}
	
	@RequestMapping(value="admin/saveEditedRoom.html")
	public String saveEditedRoom(@RequestParam(value="idHotel", required=true) Integer idHotel, Room room, ModelMap map) {
		roomDAO.saveRoom(room, idHotel);
		map.put("user", userDAO.getCurrentUser());
		map.put("images", imageDAO.getImageListByIdRoom(room.getId()));
		map.put("personalRoomList", hotelDAO.getPersonalHotelList(userDAO.getCurrentUser().getId()));
		return "management/roomInManagement";
	}
	
	@RequestMapping(value="hotels/roomsInHotels.action")
	public String hotelInHotels(@RequestParam(value="id", required=true) Integer idHotel, ModelMap map) {
		map.put("listRooms", (ArrayList<Room>) roomDAO.getRoomListByIdHotel(idHotel));
		User user = null;
		try{
			user = userDAO.getCurrentUser();
		} catch (NullPointerException e){
			
		}
		map.put("user", user);
		return "hotels/roomsInHotels";
	}
	
	@RequestMapping(value="admin/addRoom.html")
	public String addRoom(@RequestParam(value="id", required=true) Integer idHotel, ModelMap map) {
		map.put("hotel", hotelDAO.getHotelById(idHotel));
		map.put("user", userDAO.getCurrentUser());
		return "management/addRoom";
	}
	

	@RequestMapping(value="admin/roomsInManagement.html")
	public String personalRoomList(@RequestParam(value="id", required=true) Integer idHotel, ModelMap map) {
		map.put("personalRoomList", roomDAO.getRoomListByIdHotel(idHotel));
		map.put("user", userDAO.getCurrentUser());
		map.put("hotel", hotelDAO.getHotelById(idHotel));
		return "management/roomsInManagement";
	}
	
	@RequestMapping(value="admin/editRoom.html")
	public String editRoom(@RequestParam(value="id", required=true) Integer idRoom, ModelMap map) {
		Room room = roomDAO.getRoomById(idRoom);
		map.put("room", room);
		map.put("idHotel", room.getHotel().getId());
		map.put("user", userDAO.getCurrentUser());
		map.put("roomType", roomDAO.getRoomById(idRoom).getType().type);
		return "management/editRoom";
	}
	
	@RequestMapping(value="hotels/roomInHotels.action")
	public String roomInHotels(@RequestParam(value="id", required=true) Integer idRoom, ModelMap map) {
		map.put("address", address);
		map.put("images", imageDAO.getImageListByIdHotel(idRoom));
		map.put("room", roomDAO.getRoomById(idRoom));
		User user = null;
		try{
			user = userDAO.getCurrentUser();
		} catch (NullPointerException e){
			
		}
		map.put("user", user);
		return "hotels/roomInHotels";
	}
	
	@ResponseBody
	@RequestMapping(value="admin/verifyRoomNumber.action")
	public String verifyNumber(@RequestParam(value="number", required=false) String number) {
		if(null == number || "".equals(number)){
			return "";
		}
		Integer num = null;
		try{
			num = Integer.parseInt(number);
		} catch (NumberFormatException e){
			System.out.println(e);
		}
		if(null == num){
			return "false";
		}
		Room room = null;
		try{
			room = roomDAO.getRoomByNumber(num);
		} catch(StaleStateException e) {
			System.out.println(e);
		}
		return (room == null) + "";
	}
	
	@ResponseBody
	@RequestMapping(value="admin/verifyRoomPrice.action")
	public String verifyPrice(@RequestParam(value="price", required=false) String price) {
		if(null == price || "".equals(price)){
			return "";
		}
		Integer num = null;
		try{
			num = Integer.parseInt(price);
		} catch (NumberFormatException e){
			System.out.println(e);
		}
		if(null == num){
			return "false";
		} else {
			return "true";
		}
	}
	
	
}
