package com.geekhub.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class LoginController{
	
	@RequestMapping(value = "login.action")
	public String login(){
		return "login";
	}
	
	@RequestMapping(value = "loginError.action")
	public String loginError(){
		return "loginError";
	}
	
	@RequestMapping(value = "/accessError.html")
	public String accessError(){
		return "accessError";
	}

}
