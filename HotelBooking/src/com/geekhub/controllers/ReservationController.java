package com.geekhub.controllers;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.geekhub.beans.Image;
import com.geekhub.beans.Reservation;
import com.geekhub.beans.Room;
import com.geekhub.beans.SearchResult;
import com.geekhub.beans.User;
import com.geekhub.services.HotelDAO;
import com.geekhub.services.ImageDAO;
import com.geekhub.services.ReservationDAO;
import com.geekhub.services.RoomDAO;
import com.geekhub.services.UserDAO;

@Controller
public class ReservationController {

	@Autowired HotelDAO hotelDAO;
	@Autowired RoomDAO roomDAO;
	@Autowired UserDAO userDAO;
	@Autowired ReservationDAO reservationDAO;
	@Autowired ImageDAO imageDAO;
	final String address = "C:/Users/SaintDaemon/git/myApp/HotelBooking/WebContent";
	
	@RequestMapping(value="search/reserveRoom.action")
	public String reserveRoom(@RequestParam(value="idRoom", required=true) Integer idRoom, ModelMap map, HttpServletRequest request) {
		map.put("room", roomDAO.getRoomById(idRoom));
		try{
			SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
			Date d1 = new Date();
			Date d2 = new Date();
			d1.setTime(((SearchResult) request.getSession().getAttribute("searchResult")).getDateIn().getTimeInMillis());
			d2.setTime(((SearchResult) request.getSession().getAttribute("searchResult")).getDateOut().getTimeInMillis());
			map.put("dateOut", sdf.format(d1));
			map.put("dateIn", sdf.format(d2));
		} catch (NullPointerException e){
			System.out.println(e);
		}
		map.put("user", userDAO.getCurrentUser());
		return "search/reserveRoom";
	}
	
	@RequestMapping(value="reservations/reservations.html")
	public String reservationList(ModelMap map){
		User user = userDAO.getCurrentUser();
		if(user != null){
			map.put("reservList", reservationDAO.getReservationListByIdUser(user.getId()));
		}
		map.put("user", user);
		return "reservations/reservations";
	}
	
	@RequestMapping(value="reservations/hotelInReservations.html")
	public String showHotel(@RequestParam(value="id", required=true) Integer idRoom, ModelMap map){
		Room room = roomDAO.getRoomById(idRoom);
		map.put("hotel", room.getHotel());
		ArrayList<Image> list = (ArrayList<Image>) imageDAO.getImageListByIdHotel(room.getHotel().getId());
		map.put("images", list);
		map.put("address", address);
		map.put("user", userDAO.getCurrentUser());
		return "reservations/hotelInReservations";
	}
	
	@RequestMapping(value="user/deleteReservation.html")
	public String deleteReservation(@RequestParam(value="id", required=true) Integer idReservation, ModelMap map){
		reservationDAO.deleteReservation(idReservation);
		User user = userDAO.getCurrentUser();
		if(user != null){
			map.put("user", user);
			map.put("reservList", reservationDAO.getReservationListByIdUser(user.getId()));
		}
		return "reservations/reservations";
	}
	
	@RequestMapping(value="admin/reservationsInManagement.html")
	public String reservationListAdmin(@RequestParam(value="id", required=true) Integer idHotel, ModelMap map){
		ArrayList<Reservation> list = (ArrayList<Reservation>) reservationDAO.getReservationList();
		Iterator<Reservation> iter = list.iterator();
		while(iter.hasNext()){
			Reservation res = iter.next();
			Room room = roomDAO.getRoomById(res.getRoom().getId());
			if(room.getHotel().getId() != idHotel){
				iter.remove();
			}
		}
		map.put("reservList", list);
		map.put("user", userDAO.getCurrentUser());
		return "management/reservationsInManagement";
	}
	
}
