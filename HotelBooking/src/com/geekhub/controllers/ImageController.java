package com.geekhub.controllers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.geekhub.beans.Hotel;
import com.geekhub.beans.Image;
import com.geekhub.beans.Room;
import com.geekhub.services.HotelDAO;
import com.geekhub.services.ImageDAO;
import com.geekhub.services.RoomDAO;
import com.geekhub.services.UserDAO;

@Controller
public class ImageController {
	
	@Autowired ImageDAO imageDAO;
	@Autowired HotelDAO hotelDAO;
	@Autowired RoomDAO roomDAO;
	@Autowired UserDAO userDAO;
	final String address = "C:\\Users\\SaintDaemon\\git\\myApp\\HotelBooking\\WebContent";
	
	@RequestMapping(value="admin/addImage.html")
	public String addImage(@RequestParam(value="idHotel", required=false) Integer idHotel, @RequestParam(value="idRoom", required=false) Integer idRoom, ModelMap map) {
		if(idHotel != null){
			map.put("hotel", hotelDAO.getHotelById(idHotel));
		}
		if(idRoom != null){
			map.put("room", roomDAO.getRoomById(idRoom));
		}
		map.put("user", userDAO.getCurrentUser());
		return "management/addImage";
	}
	
	@RequestMapping(value="admin/deleteImage.html")
	public String deleteImage(@RequestParam(value="id", required=true) Integer id, ModelMap map) {
		map.put("user", userDAO.getCurrentUser());
		Room room = imageDAO.getImageById(id).getRoom();
		Hotel hotel = imageDAO.getImageById(id).getHotel();
		if(hotel != null){
			map.put("hotel", hotelDAO.getHotelById(hotel.getId()));
			imageDAO.deleteImage(id);
			map.put("images", imageDAO.getImageListByIdHotel(hotel.getId()));
			return "management/hotelInManagement";
		} else{
			map.put("room", roomDAO.getRoomById(room.getId()));
			imageDAO.deleteImage(id);
			map.put("images", imageDAO.getImageListByIdRoom(room.getId()));
			return "management/roomInManagement";
		}
		
	}
	
	@RequestMapping(value="admin/saveImage.html")
	public String saveImage(@RequestParam(value="file", required=true) MultipartFile file,
			@RequestParam(value="idHotel", required=false) Integer idHotel, 
			@RequestParam(value="idRoom", required=false) Integer idRoom, ModelMap map) throws IOException {
		
		Image img = new Image();
		img.setContent(file.getOriginalFilename());
		if(idHotel != null){
			img.setHotel(hotelDAO.getHotelById(idHotel));
		}
		if(idRoom != null){
			img.setRoom(roomDAO.getRoomById(idRoom));
		}
		List<Image> list = imageDAO.getImageListBy();
		if(!list.contains(img)){
			File dir = new File(address + "\\images\\");
			dir.mkdir(); 
			dir.mkdirs();
			
			FileInputStream f = (FileInputStream) file.getInputStream();
			FileOutputStream f2 = new FileOutputStream(dir + "\\" + file.getOriginalFilename());
			int i;
			do{
				i = f.read();
				if(i != -1){
					f2.write(i);
				}
			} while(i != -1);
			f.close();
			f2.close();
			imageDAO.saveImage(img, idRoom, idHotel);
		}
		
		map.put("user", userDAO.getCurrentUser());
		if(idHotel != null){
			map.put("hotel", hotelDAO.getHotelById(idHotel));
			map.put("images", imageDAO.getImageListByIdHotel(idHotel));
			return "management/hotelInManagement";
		}
		map.put("room", roomDAO.getRoomById(idRoom));
		map.put("images", imageDAO.getImageListByIdRoom(idRoom));
		return "management/roomInManagement";
	}
	
}