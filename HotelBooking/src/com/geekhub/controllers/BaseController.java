package com.geekhub.controllers;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;

import org.quartz.SchedulerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.geekhub.beans.Reservation;
import com.geekhub.services.HotelDAO;
import com.geekhub.services.ReservationDAO;
import com.geekhub.services.UserDAO;

@Controller
public class BaseController{
	
	@Autowired HotelDAO hotelDAO;
	@Autowired UserDAO userDAO;
	@Autowired ReservationDAO reservationDAO;
	
	private Thread thread;
	private boolean checkingReservation = false;
	
	@RequestMapping(value="index.action")
	public String index(ModelMap map) throws ParseException, SchedulerException {
		if(checkingReservation == false){
			checkingReservation();
			thread.start();
			checkingReservation = true;
		}
		map.put("hotels", hotelDAO.getHotelList().size());
		map.put("user", userDAO.getCurrentUser());
		return "index";
	}
	
	public void checkingReservation(){
		thread = new Thread(new Runnable(){

			@SuppressWarnings("deprecation")
			@Override
			public void run() {
				while(true){
					System.out.println("Start checking Reservationend!");
					ArrayList<Reservation> rList = null;
					try {
						rList = (ArrayList<Reservation>) reservationDAO.getReservationList();
					} catch (NullPointerException e) {
						System.out.println(e);
					}
					if(rList != null){
						Calendar c = Calendar.getInstance();	
						Iterator<Reservation> iter = rList.iterator();
						while(iter.hasNext()){
							Reservation r = iter.next();
							if(c.get(Calendar.YEAR) > (r.getDateOut().getYear()+1900) || 
									(c.get(Calendar.YEAR) == (r.getDateOut().getYear()+1900) && c.get(Calendar.MONTH) > r.getDateOut().getMonth()) ||
									(c.get(Calendar.YEAR) == (r.getDateOut().getYear()+1900) && c.get(Calendar.MONTH) == r.getDateOut().getMonth() && c.get(Calendar.DATE) > r.getDateOut().getDate())){
								r.setTopicality(false);
								Integer idUser = null;
								try{
									idUser = r.getUser().getId();
								} catch (NullPointerException e) {
									System.out.println(e);
								}
								reservationDAO.saveReservation(r, r.getRoom().getId(), idUser);
							}
						}
					}
					System.out.println("End of the test Reservation!");
					try {
						Thread.sleep(24000*60*360);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
			
		});
	}

}
