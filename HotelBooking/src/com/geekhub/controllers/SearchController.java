package com.geekhub.controllers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.geekhub.beans.Hotel;
import com.geekhub.beans.Reservation;
import com.geekhub.beans.Room;
import com.geekhub.beans.SearchResult;
import com.geekhub.services.HotelDAO;
import com.geekhub.services.ReservationDAO;
import com.geekhub.services.RoomDAO;
import com.geekhub.services.UserDAO;

@Controller
public class SearchController {
	
	@Autowired HotelDAO hotelDAO;
	@Autowired RoomDAO roomDAO;
	@Autowired UserDAO userDAO;
	@Autowired ReservationDAO reservationDAO;
	
	private String sort = "asc";
	
	public String getSort() {
		return sort;
	}
	public void setSort(String sort) {
		this.sort = sort;
	}

	public boolean checkingDate(Reservation r, Calendar dateIn, Calendar dateOut){
		Calendar calendarIn = Calendar.getInstance();
		Calendar calendarOut = Calendar.getInstance();
		calendarIn.setTime(r.getDateIn());
		calendarOut.setTime(r.getDateOut());
		if(dateOut.getTimeInMillis() < calendarIn.getTimeInMillis()){
			return true;
		} else {
			if(dateIn.getTimeInMillis() > calendarOut.getTimeInMillis()){
				return true;
			} else {
				return false;
			}
		}

	}
	
	SearchResult checking(ArrayList<Hotel> list, String type, Calendar dateIn, Calendar dateOut, HttpSession session){
		SearchResult searchResult = new SearchResult();
		HashMap<Integer, ArrayList<Room>> roomMap = new HashMap<Integer, ArrayList<Room>>();
		Iterator<Hotel> iterH = list.iterator();
		while(iterH.hasNext()){
			Hotel hotel = iterH.next();
			ArrayList<Room> listRooms = (ArrayList<Room>) roomDAO.getRoomListByIdHotel(hotel.getId());
			if(listRooms == null || listRooms.size() == 0){
				iterH.remove();
			} else {
				Iterator<Room> iterR = listRooms.iterator();
				while(iterR.hasNext()){
					Room room = iterR.next();
					if(room.isAccess() == false){
						iterR.remove();
					} else {
						if((type != null) && (!room.getType().name().equals(type))) {
							iterR.remove();
						} else {
							if (dateIn != null && dateOut != null){
								System.out.println("asd");
								searchResult.setDateIn(dateIn);
								searchResult.setDateOut(dateOut);
								ArrayList<Reservation> listR = (ArrayList<Reservation>) reservationDAO.getReservationListByIdRoom(room.getId());
								for(Reservation r: listR){
									if(checkingDate(r, dateIn, dateOut) == false){
										iterR.remove();
										break;
									}
								}
							}
						}
					}
				}
				if(listRooms.size() == 0){
					iterH.remove();
				} else {
					roomMap.put(hotel.getId(), listRooms);
				}
			}
			searchResult.setHotelList(list);
			searchResult.setRoomMap(roomMap);
			session.setAttribute("searchResult", searchResult);
		}
		return searchResult;
	}
	
	@RequestMapping(value="search/search.action")
	public String search(@RequestParam(value="name/place", required=true) String namePlace,
			@RequestParam(value="type", required=true) String type,
			@RequestParam(value="datepicker", required=true) String date,
			@RequestParam(value="datepicker2", required=true) String date2, ModelMap map, HttpServletRequest request) throws ParseException{
		
		Date dateIn = null;
		Date dateOut = null;
		Calendar calendarIn = null;
		Calendar calendarOut = null;
		
		if(date.length() > 0){
			SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
			dateIn = sdf.parse(date);
			dateOut = sdf.parse(date2);
			calendarIn = new GregorianCalendar();
			calendarOut = new GregorianCalendar();
			calendarIn.setTime(dateIn);
			calendarOut.setTime(dateOut);
		}
		
		ArrayList<Hotel> list = null;
		
		if(!"".equals(namePlace)){
			if(!"type".equals(type)){
				//name + type + date
				list = (ArrayList<Hotel>) hotelDAO.getHotelListByNamePlace(namePlace);
				list = checking(list, type, calendarIn, calendarOut, request.getSession()).getHotelList();
			} else {
				//name + date
				list = (ArrayList<Hotel>) hotelDAO.getHotelListByNamePlace(namePlace);
				list = checking(list, null, calendarIn, calendarOut, request.getSession()).getHotelList();
			}
		} else {
			if(!"type".equals(type)){
				//type + date
				list = (ArrayList<Hotel>) hotelDAO.getHotelList();
				list = checking(list, type, calendarIn, calendarOut, request.getSession()).getHotelList();
			} else {
				//date
				list = (ArrayList<Hotel>) hotelDAO.getHotelList();
				list = checking(list, null, calendarIn, calendarOut, request.getSession()).getHotelList();
			}
		}
		map.put("user", userDAO.getCurrentUser());
		if(list != null && list.size() != 0){
			map.put("hotels", list);
			return "search/hotelsInSearch";
		} else {
			return "search/noSearchResults";
		}
	}
	
	@RequestMapping(value="search/finalCheck.action")
	public String finalCheck(@RequestParam(value="datepicker", required=true) String date,
			@RequestParam(value="datepicker2", required=true) String date2,
			@RequestParam(value="idRoom", required=true) Integer idRoom,
			@RequestParam(value="idUser", required=true) Integer idUser, Reservation reservation, ModelMap map, HttpServletRequest request) throws ParseException{
		
		Date dateIn = null;
		Date dateOut = null;
		Calendar calendarIn = null;
		Calendar calendarOut = null;
		
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		if(date.length() > 0){
			dateIn = sdf.parse(date);
			dateOut = sdf.parse(date2);
			calendarIn = Calendar.getInstance();
			calendarOut = Calendar.getInstance();
			calendarIn.setTime(dateIn);
			calendarOut.setTime(dateOut);
		}
		
		ArrayList<Reservation> reservations = (ArrayList<Reservation>) reservationDAO.getReservationListByIdRoom(idRoom);
		
		for(Reservation r: reservations){
			if(checkingDate(r, calendarIn, calendarOut) == false){
				map.put("user", userDAO.getCurrentUser());
				map.put("room", roomDAO.getRoomById(idRoom));
				try{
					map.put("dateIn", sdf.format(dateIn));
					map.put("dateOut", sdf.format(dateOut));
				} catch (NullPointerException e){
					System.out.println(e);
				}
				return "search/reserveRoomError";
			}
		}
		reservation.setDateIn(dateIn);
		reservation.setDateOut(dateOut);
		reservationDAO.saveReservation(reservation, idRoom, idUser);
		map.put("user", userDAO.getCurrentUser());
		return "successOperation";
	}
	
	@RequestMapping(value="search/sort.action")
	public String sort(HttpServletRequest request, ModelMap map){
		SearchResult sr = (SearchResult) request.getSession().getAttribute("searchResult");
		ArrayList<Hotel> list = sr.getHotelList();
		
		if("asc".equals(sort)){
			Collections.sort(list);
			sort = "desc";
		} else {
			Collections.sort(list, new Comparator<Hotel>(){
				@Override
				public int compare(Hotel h1, Hotel h2) {
					if(h1.getPriority().priority < h2.getPriority().priority){
						return 1;
					} else {
						if(h1.getPriority().priority == h2.getPriority().priority){
							return 0;
						} else {
							return -1;
						}
					}
				}
			});
			sort = "asc";
		}
		map.put("hotels", list);
		map.put("user", userDAO.getCurrentUser());
		return "search/hotelsInSearch";
	}
	
}	