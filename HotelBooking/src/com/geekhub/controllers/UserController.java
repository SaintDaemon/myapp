package com.geekhub.controllers;

import org.hibernate.StaleStateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.geekhub.beans.User;
import com.geekhub.services.UserDAO;

@Controller
public class UserController {
	
	@Autowired UserDAO userDAO;
	
	@RequestMapping(value="user/accountEdit.html")
	public String accountEdit(@RequestParam(value="id", required=false) Integer idUser, ModelMap map) {
		User user = userDAO.getUserById(idUser);
		map.put("user", user);
		map.put("user", userDAO.getUserById(idUser));
		return "accountEdit";
	}
	
	@RequestMapping(value="saveUser.action")
	public String saveUser(@RequestParam(value="password", required=true) String password, User user, ModelMap map) {
		user.setPassword(userDAO.encryptString(password));
		userDAO.saveUser(user);
		map.put("msg", "Registration is successful");
		return "successOperation";
	}
	
	@RequestMapping(value="user/saveEditedAccount.html")
	public String saveEditedAccount(@RequestParam(value="password", required=false) String password, @RequestParam(value="id", required=true) Integer id, User user, ModelMap map) {
		if(password != null && !"".equals(password)){
			user.setPassword(userDAO.encryptString(password));
		} else {
			user.setPassword(userDAO.getUserById(id).getPassword());
		}
		user.setStatus(userDAO.getUserById(id).getStatus());
		if(!user.getUsername().equals(userDAO.getUserById(id).getUsername())){
			SecurityContextHolder.getContextHolderStrategy().clearContext();
			userDAO.saveUser(user);
			return "index";
		}
		userDAO.saveUser(user);
		map.put("user", userDAO.getCurrentUser());
		return "account";
	}
	
	@RequestMapping(value="signUp.action")
	public String signUp(ModelMap map) {
		map.put("user", userDAO.getCurrentUser());
		return "signUp";
	}
	
	@RequestMapping(value="user/account.html")
	public String myAccount(ModelMap map) {
		map.put("user", userDAO.getCurrentUser());
		return "account";
	}
	
	@ResponseBody
	@RequestMapping(value="/verify.action")
	public String verifySignUp(@RequestParam(value="username", required=true) String username) {
		if("".equals(username)){
			return "";
		}
		User user = null;
		try{
			user = userDAO.getUserByUsername(username);
		} catch(StaleStateException e) {
			System.out.println(e);
		}
		return (user == null) + "";
	}
	
}
