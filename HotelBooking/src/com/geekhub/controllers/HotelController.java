package com.geekhub.controllers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.geekhub.beans.Hotel;
import com.geekhub.beans.User;
import com.geekhub.services.HotelDAO;
import com.geekhub.services.ImageDAO;
import com.geekhub.services.RoomDAO;
import com.geekhub.services.UserDAO;

@Controller
public class HotelController {

	@Autowired HotelDAO hotelDAO;
	@Autowired RoomDAO roomDAO;
	@Autowired UserDAO userDAO;
	@Autowired ImageDAO imageDAO;
	final String address = "C:/Users/SaintDaemon/git/myApp/HotelBooking/WebContent";
	
	private String sort = "asc";
	
	@RequestMapping(value="admin/deleteHotel.html")
	public String deleteHotel(@RequestParam(value="idHotel", required=true) Integer idHotel,
			@RequestParam(value="password", required=false) String password,
			@RequestParam(value="idUser", required=true) Integer idUser, ModelMap map) {
		if((password == null) || !(userDAO.encryptString(password).equals(userDAO.getUserById(idUser).getPassword()))){
			map.put("hotel", hotelDAO.getHotelById(idHotel));
			map.put("user", userDAO.getCurrentUser());
			return "management/deleteHotelCheckWrong";
		} else {
			hotelDAO.deleteHotel(idHotel);
			map.put("user", userDAO.getCurrentUser());
			map.put("personalHotelList", hotelDAO.getPersonalHotelList(idUser));
			return "management/hotelsInManagement";
		}
	}
	
	@RequestMapping(value="admin/deleteHotelCheck.html")
	public String deleteHotelCheck(@RequestParam(value="idHotel", required=true) Integer idHotel, ModelMap map) {
		map.put("hotel", hotelDAO.getHotelById(idHotel));
		map.put("user", userDAO.getCurrentUser());
		return "management/deleteHotelCheck";
	}
	
	@RequestMapping(value="admin/saveHotel.html")
	public String saveHotel(@RequestParam(value="idUser", required=true) Integer idUser, Hotel hotel, ModelMap map) {
		User owner = userDAO.getCurrentUser();
		hotel.setPriorityView(hotel.getPriority());
		hotel.setOwner(owner);
		hotelDAO.saveHotel(hotel, idUser);
		map.put("user", userDAO.getCurrentUser());
		map.put("images", imageDAO.getImageListByIdHotel(hotel.getId()));
		map.put("personalHotelList", hotelDAO.getPersonalHotelList(idUser));
		return "management/hotelsInManagement";
	}
	
	@RequestMapping(value="admin/editHotel.html")
	public String editHotel(@RequestParam(value="id", required=true) Integer idHotel, ModelMap map) {
		map.put("hotel", hotelDAO.getHotelById(idHotel));
		map.put("hotelPriority", hotelDAO.getHotelById(idHotel).getPriority().priority);
		map.put("user", userDAO.getCurrentUser());
		return "management/editHotel";
	}
	
	@RequestMapping(value="admin/saveEditedHotel.html")
	public String saveEditedHotel(@RequestParam(value="idUser", required=true) Integer idUser, Hotel hotel, ModelMap map) {
		User owner = userDAO.getCurrentUser();
		hotel.setPriorityView(hotel.getPriority());
		hotel.setOwner(owner);
		hotelDAO.saveHotel(hotel, idUser);
		map.put("user", userDAO.getCurrentUser());
		map.put("images", imageDAO.getImageListByIdHotel(hotel.getId()));
		map.put("personalHotelList", hotelDAO.getPersonalHotelList(idUser));
		return "management/hotelInManagement";
	}
	
	@RequestMapping(value="admin/addHotel.html")
	public String addHotel(ModelMap map) {
		map.put("user", userDAO.getCurrentUser());
		return "management/addHotel";
	}
	
	@RequestMapping(value="admin/hotelInManagement.html")
	public String hotelInManagement(@RequestParam(value="id", required=true) Integer idHotel, ModelMap map) {
		Hotel hotel = hotelDAO.getHotelById(idHotel);
		map.put("hotel", hotel);
		map.put("images", imageDAO.getImageListByIdHotel(idHotel));
		map.put("address", address);
		map.put("user", userDAO.getCurrentUser());
		return "management/hotelInManagement";
	}
	
	@RequestMapping(value="reservations/user/hotelInReservations.html")
	public String hotelInReservations(@RequestParam(value="id", required=true) Integer idHotel, ModelMap map) {
		Hotel hotel = hotelDAO.getHotelById(idHotel);
		map.put("hotel", hotel);
		map.put("images", imageDAO.getImageListByIdHotel(idHotel));
		map.put("address", address);
		map.put("user", userDAO.getCurrentUser());
		return "reservations/hotelInReservations";
	}
	
	@RequestMapping(value="admin/hotelsInManagement.html")
	public String hotelsInManagement(@RequestParam(value="id", required=true) Integer idUser, ModelMap map) {
		map.put("personalHotelList", hotelDAO.getPersonalHotelList(idUser));
		map.put("user", userDAO.getCurrentUser());
		return "management/hotelsInManagement";
	}
	
	@RequestMapping(value="search/hotelInSearch.action")
	public String hotelInSearch(@RequestParam(value="id", required=true) Integer idHotel, ModelMap map) {
		map.put("hotel", hotelDAO.getHotelById(idHotel));
		map.put("address", address);
		map.put("images", imageDAO.getImageListByIdHotel(idHotel));
		map.put("user", userDAO.getCurrentUser());
		return "search/hotelInSearch";
	}
	
	@RequestMapping(value="hotels/hotels.action")
	public String hotels(ModelMap map) {
		map.put("user", userDAO.getCurrentUser());
		ArrayList<Hotel> list = (ArrayList<Hotel>) hotelDAO.getHotelList();
		if(list != null && list.size() != 0){
			map.put("hotels", list);
			return "hotels/hotels";
		} else {
			return "hotels/noHotels";
		}
	}
	
	@RequestMapping(value="hotels/hotelInHotels.action")
	public String hotelInHotels(@RequestParam(value="id", required=true) Integer idHotel, ModelMap map) {
		map.put("hotel", hotelDAO.getHotelById(idHotel));
		map.put("address", address);
		map.put("images", imageDAO.getImageListByIdHotel(idHotel));
		User user = null;
		try{
			user = userDAO.getCurrentUser();
		} catch (NullPointerException e){
			
		}
		map.put("user", user);
		return "hotels/hotelInHotels";
	}
	
	@RequestMapping(value="hotels/sortHotels.action")
	public String sortHotels(HttpServletRequest request, ModelMap map){
		ArrayList<Hotel> list = (ArrayList<Hotel>) hotelDAO.getHotelList();
		
		if("asc".equals(sort)){
			Collections.sort(list);
			sort = "desc";
		} else {
			Collections.sort(list, new Comparator<Hotel>(){
				@Override
				public int compare(Hotel h1, Hotel h2) {
					if(h1.getPriority().priority < h2.getPriority().priority){
						return 1;
					} else {
						if(h1.getPriority().priority == h2.getPriority().priority){
							return 0;
						} else {
							return -1;
						}
					}
				}
			});
			sort = "asc";
		}
		map.put("hotels", list);
		map.put("user", userDAO.getCurrentUser());
		return "hotels/hotels";
	}
	
}
