package BeanRepresenterWithAnnotation;

public class Cat {
	
	private String name;
	@MyAnnotation(type = "skip")
	private int age;
	private Human owner;
	
	public Cat(String name, int age, Human owner) {
		this.name = name;
		this.age = age;
		this.owner = owner;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public Human getOwner() {
		return owner;
	}
	public void setOwner(Human owner) {
		this.owner = owner;
	}
}
