package BeanRepresenterWithAnnotation;

public class Car {

	private String mark;
	@MyAnnotation(type = "skip")
	private String color;	
	private int year;
	
	public Car(String mark, String color, int year) {
		this.mark = mark;
		this.year = year;
		this.color = color;
	}
	
	public String getMark() {
		return mark;
	}
	public void setMark(String mark) {
		this.mark = mark;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}	
}
