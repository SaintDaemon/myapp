package BeanRepresenterWithAnnotation;

import java.lang.reflect.Field;
import BeanRepresenterWithAnnotation.MyAnnotation;;

// class that implements present method
public class Representer {

	// method that implements output object data of unknown class
	static void present(Object o, String retreat) throws IllegalArgumentException, IllegalAccessException {
		Class clazz = o.getClass();
		System.out.println(retreat + clazz.getSimpleName());
		System.out.println(retreat + "____");
		Field[] fields = clazz.getDeclaredFields();
		for (Field f : fields) {
			MyAnnotation ma = f.getAnnotation(MyAnnotation.class);
			if (ma == null || !(ma.type().equals("skip"))) {
				f.setAccessible(true);
				if (f.toString().indexOf("String") != -1) {
					System.out.print(retreat + "| String");
					System.out.print("   " + f.getName());
					Object value = f.get(o);
					System.out.print("   " + value + "\n");
				} else {
					if (f.getType().isPrimitive()) {
						System.out.print(retreat + "| " + f.getType());
						System.out.print("   " + f.getName());
						Object value = f.get(o);
						System.out.print("   " + value + "\n");
					} else {
						System.out.print(retreat + "|" + f.getName() + "\n");
						present(f.get(o), retreat + "   ");
					}
				}
			}
		}
	}

	public static void main(String args[]) throws IllegalArgumentException,
			IllegalAccessException {
		present(new Cat("Tom", 3, new Human("Chack", 90, new Car("Lambo",
				"red", 2012))), "");
	}
}
