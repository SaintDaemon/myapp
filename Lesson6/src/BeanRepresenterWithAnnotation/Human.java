package BeanRepresenterWithAnnotation;

public class Human {
	private String name;
	@MyAnnotation(type = "skip")
	private int age;
	private Car car;
	
	public Human(String name, int age, Car car) {
		this.name = name;
		this.age = age;
		this.car = car;
	}
	
	public Car getCar() {
		return car;
	}
	public void setCar(Car car) {
		this.car = car;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}	
}