package BeanRepresenterWithAnnotation;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)

public @interface MyAnnotation {
	String type();
}
