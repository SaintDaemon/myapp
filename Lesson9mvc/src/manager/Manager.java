package manager;

import java.util.Enumeration;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class Manager {

	@RequestMapping (value="/animals.page")
	protected ModelAndView handleRequestInternal(HttpServletRequest req) throws ServletException{
		ModelAndView mv = new ModelAndView("index");
		HttpSession session = get(req);
		Enumeration<String> values = session.getAttributeNames();
		HashMap<String, String> animalList = new HashMap<>();
		while(values.hasMoreElements()){ 
			String key = (String) values.nextElement();
			String val = (String) session.getAttribute(key);
			animalList.put(key, val);
		}
		mv.addObject("animalList", animalList);
		return mv;
	}
	
	protected HttpSession get(HttpServletRequest req) throws ServletException {
		HttpSession session = req.getSession();
    	String del = req.getParameter("del");
	 	if(del != null){
			session.removeAttribute(del);
	 	} else {
	 		String type = req.getParameter("type");
	 		String name = req.getParameter("name");
	 		if(type != null && name != null){
	 			if(!"".equals(type) && !"".equals(name)){
	 				session.setAttribute(name, type);
	 			}
	 		}
	 	}
	 	return session;
	}
	
}
