<%@page import="java.util.Enumeration"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Lesson 9 MVC</title>
</head>
<body>
	<form action="/Lesson9mvc/animals.page">
		<table border=3>
			<tr>
				<td>Type</td>
				<td>Name</td>
			</tr>				 
    			<c:forEach var="list" items="${animalList}">
    		<tr>
    			<td>${list.key}</td>
    			<td>${list.value}</td>
    			<td><a href="animals.page?del=${list.key}">delete</a></td>
    		</tr>
    			</c:forEach>
			<tr>
				<td><input type="text" name="name" value=""></td>
				<td><input type="text" name="type" value=""></td>
				<td><input type=submit value="add"></td>
			</tr>	
		</table>
	</form>
</body>
</html>