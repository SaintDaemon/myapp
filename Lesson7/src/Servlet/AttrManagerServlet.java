package Servlet;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class Servlet
 */
public class AttrManagerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AttrManagerServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	void writeSession(HttpServletResponse response, HttpSession session) throws IOException {
		String resultStr = "";
		@SuppressWarnings("rawtypes")
		Enumeration values = session.getAttributeNames();
		while (values.hasMoreElements()) {
			String key = (String) values.nextElement();
			String val = (String) session.getAttribute(key);
			resultStr += key + " - " + val + "<br>";
		}
		response.getWriter().write(resultStr);
	}

	void writeServerContext(HttpServletResponse response, ServletContext sc) throws IOException {
		String resultStr = "";
		@SuppressWarnings("rawtypes")
		Enumeration values = sc.getAttributeNames();
		while (values.hasMoreElements()) {
			String key = values.nextElement().toString();
			String val = sc.getAttribute(key).toString();
			resultStr += key + " - " + val + "<br>";
		}
		response.getWriter().write(resultStr);
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String action = request.getParameter("action");
		String name = request.getParameter("name");
		String value = request.getParameter("value");
		String message = request.getParameter("message");
		String scope = request.getParameter("scope");

		HttpSession session = request.getSession();
		ServletContext sc = request.getServletContext();

		if ("session".equals(scope)) {
			if ("add".equals(action) || "update".equals(action)) {
				response.getWriter().write("Message: " + message + "<br>");
				session.setAttribute(name, value);
			} else if ("remove".equals(action)) {
				response.getWriter().write("Message: " + message + "<br>");
				session.removeAttribute(name);
			} else if ("invalidate".equals(action)) {
				session.invalidate();
			}
		} else if("app".equals(scope)) {
			if ("add".equals(action) || "update".equals(action)) {
				response.getWriter().write("Message: " + message + "<br>");
				sc.setAttribute(name, value);
			} else if ("remove".equals(action)) {
				response.getWriter().write("Message: " + message + "<br>");
				sc.removeAttribute(name);
			}
		}
		writeSession(response, session);
		writeServerContext(response, sc);
	}

}
