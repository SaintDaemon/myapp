<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<form action="saveTicket.html">
    Title <input type="text" value="${ticket.title}" name="title"><br>
    Description <input type="text" value="${ticket.description}" name="description"><br>
    
    Status 
    <select name="status">
        <option value="ACTIVE">Active</option>
        <option value="RESOLVED">Resolved</option>
        <option value="TESTED">Tested</option>
        <option value="PUSHED">Pushed</option>
    </select><br>

    Priority
    <select name="priority">
        <option value="LOW">Low</option>
        <option value="NORMAL">Normal</option>
        <option value="HIGH">High</option>
    </select><br>

    Owner
    <select name="owner">
    <c:forEach items="${users}" var="user">
        <option value="${user.id}">${user.name} (${user.id})</option>
    </c:forEach>
    </select><br>
    
    <input type="hidden" name="id" value="${ticket.id}">
    <input type="submit" value="Save">
    
    <p><a href="index.html">Home</a></p>
</form>