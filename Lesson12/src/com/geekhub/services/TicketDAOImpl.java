package com.geekhub.services;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.geekhub.beans.Ticket;

@Repository
@Transactional
public class TicketDAOImpl implements TicketDAO {

	@Autowired SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	
	@Override
	public List<Ticket> getTicketList() {
		return sessionFactory.getCurrentSession().createQuery("from Ticket").list();
	}

	@Override
	public Ticket getTicketById(Integer id) {
		return (Ticket) sessionFactory.getCurrentSession().get(Ticket.class, id);
	}

	@Override
	public void saveTicket(Ticket ticket) {
		sessionFactory.getCurrentSession().saveOrUpdate(ticket);
	}

}
