package com.geekhub.services;

import java.util.List;

import com.geekhub.beans.Ticket;

public interface TicketDAO {

	public List<Ticket> getTicketList();
	
	public Ticket getTicketById(Integer id);
	
	public void saveTicket(Ticket ticket);
}
