package com.geekhub.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.geekhub.beans.Ticket;
import com.geekhub.services.TicketDAO;
import com.geekhub.services.UserDAO;

@Controller
public class TicketController {
	
	@Autowired TicketDAO ticketDAO;
	@Autowired UserDAO userDAO;
	
	@RequestMapping(value="listTickets.html")
	public String list(ModelMap map) {
		map.put("tickets",ticketDAO.getTicketList());
		return "tickets";
	}
	
	@RequestMapping(value="loadTicket.html")
	public String load(@RequestParam(value="id", required=false) Integer id, ModelMap map) {
		Ticket ticket = id == null ? new Ticket() : ticketDAO.getTicketById(id);
		map.put("ticket", ticket);
		map.put("users", userDAO.getUserList());
		return "ticket";
	}
	
	@RequestMapping(value="saveTicket.html")
	public String save(Ticket ticket) {
		ticketDAO.saveTicket(ticket);
		return "redirect:listTickets.html";
	}

}
