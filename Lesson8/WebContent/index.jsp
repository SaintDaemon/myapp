<%@page import="java.util.Enumeration"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<form name="Form1" action="My_Servlet">
		<table border=3>
			<tr>
				<td>Type</td>
				<td>Name</td>
			</tr>				
				<c:set var="resultStr" value=""></c:set>
    			<%
    			Enumeration<String> values = session.getAttributeNames();
    			while(values.hasMoreElements()){ 
    				String key = (String) values.nextElement();
    				String val = (String) session.getAttribute(key);%>
    		<tr>
    			<td><%= key %></td>
    			<td><%= val %></td>
    			<td><a href="My_Servlet?del=<%= key %>">delete</a></td>
    		</tr>
    			<%}%>
			<tr>
				<td><input type="text" name="name" value=""></td>
				<td><input type="text" name="type" value=""></td>
				<td><input type=submit value="add"></td>
			</tr>	
		</table>
	</form>
</body>
</html>