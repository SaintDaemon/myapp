import java.io.*;

public class CharSequence {
	// �����, �� �������� ������������ ������� � m �� n
	void showCharSequence(char m, char n) {
		if (m < n) {
			for (char i = m; i <= n; i++) {
				System.out.println(i);
			}
		} else {
			for (char i = m; i >= n; i--) {
				System.out.println(i);
			}
		}
	}

	public static void main(String args[]) throws IOException {
		// ���������� ���� �������
		BufferedReader br1 = new BufferedReader(
				new InputStreamReader(System.in));
		System.out.println("������ ������ ������: ");
		char m = (char) br1.read();

		BufferedReader br2 = new BufferedReader(
				new InputStreamReader(System.in));
		System.out.println("������ ������ �������: ");
		char n = (char) br2.read();

		CharSequence chS = new CharSequence();
		chS.showCharSequence(m, n);

	}
}
