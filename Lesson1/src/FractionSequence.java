import java.text.NumberFormat;
import java.util.Scanner;

public class FractionSequence {
    
    void showFractionSequence(int n){
    	NumberFormat nf = NumberFormat.getInstance();
		nf.setMinimumFractionDigits(n);
		System.out.println(nf.format((double) 1/n));
	}
    
    
    public static void main(String args[]){
        //���������� n
        Scanner sc1 = new Scanner(System.in);
        System.out.println("������ n: ");
        int n=sc1.nextInt();
        
        FractionSequence frS = new FractionSequence();//��������� ���������� �����
        frS.showFractionSequence(n);//������������ ������
    }
}
