import java.io.*;

public class FibonacciSequence {
    //����� ��� ��������� ����� Գ�������
    void showFibonacciSequence(int n){
        int f0=0, f1=1, f;
        if(n>0){
            System.out.print(f0+" ");
            for(int i=0;i<n-1;i++){
                System.out.print(f1+" ");
                f=f0;
                f0=f1;
                f1=f1+f;
            }
        }
    }
    
    //����� ��� ���������� �����
    int readNumber() throws IOException{
        int n=0;
        boolean a=false;
        while(a==false){
            BufferedReader br1 = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("������ n: ");
            try{
                String s = br1.readLine();
                n=Integer.parseInt(s);
                a=true;
            }
            catch(NumberFormatException  e){
                System.out.println("������� ������ �����!");
            }
        }
        return n;
    }
    
    public static void main(String args[]) throws IOException{
        FibonacciSequence f = new FibonacciSequence();
        int x = f.readNumber();
        f.showFibonacciSequence(x);
    }
}
